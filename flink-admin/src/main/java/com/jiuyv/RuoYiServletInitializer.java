package com.jiuyv;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @Description: web容器中进行部署
 * @author shu_k
 * @date 2021年3月30日 下午3:29:06
 */
public class RuoYiServletInitializer extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(RuoYiApplication.class);
	}
}
