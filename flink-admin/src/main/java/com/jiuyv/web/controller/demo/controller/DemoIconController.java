package com.jiuyv.web.controller.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Description: 图标相关
 * @author  shu_k
 * @date 2021年3月30日 下午3:32:47
 */
@Controller
@RequestMapping("/demo/icon")
public class DemoIconController {
	private String prefix = "demo/icon";

	/**
	 * FontAwesome图标
	 */
	@GetMapping("/fontawesome")
	public String fontAwesome() {
		return prefix + "/fontawesome";
	}

	/**
	 * Glyphicons图标
	 */
	@GetMapping("/glyphicons")
	public String glyphicons() {
		return prefix + "/glyphicons";
	}
}
