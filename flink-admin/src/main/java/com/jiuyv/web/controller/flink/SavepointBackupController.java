package com.jiuyv.web.controller.flink;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jiuyv.common.annotation.Log;
import com.jiuyv.common.enums.BusinessType;
import com.jiuyv.flink.domain.SavepointBackup;
import com.jiuyv.flink.service.ISavepointBackupService;
import com.jiuyv.common.core.controller.BaseController;
import com.jiuyv.common.core.domain.AjaxResult;
import com.jiuyv.common.utils.poi.ExcelUtil;
import com.jiuyv.common.core.page.TableDataInfo;

/**
 * savepoint备份地址Controller
 * 
 * @author shu_k
 * @date 2021-04-01
 */
@Controller
@RequestMapping("/flink/savepoint")
public class SavepointBackupController extends BaseController {
	private String prefix = "flink/savepoint";

	@Autowired
	private ISavepointBackupService savepointBackupService;

	@RequiresPermissions("flink:savepoint:view")
	@GetMapping()
	public String savepoint() {
		return prefix + "/savepoint";
	}

	/**
	 * 查询savepoint备份地址列表
	 */
	@RequiresPermissions("flink:savepoint:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(SavepointBackup savepointBackup) {
		startPage();
		List<SavepointBackup> list = savepointBackupService.selectSavepointBackupList(savepointBackup);
		return getDataTable(list);
	}

	/**
	 * 导出savepoint备份地址列表
	 */
	@RequiresPermissions("flink:savepoint:export")
	@Log(title = "savepoint备份地址", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(SavepointBackup savepointBackup) {
		List<SavepointBackup> list = savepointBackupService.selectSavepointBackupList(savepointBackup);
		ExcelUtil<SavepointBackup> util = new ExcelUtil<SavepointBackup>(SavepointBackup.class);
		return util.exportExcel(list, "savepoint");
	}

	/**
	 * 新增savepoint备份地址
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存savepoint备份地址
	 */
	@RequiresPermissions("flink:savepoint:add")
	@Log(title = "savepoint备份地址", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(SavepointBackup savepointBackup) {
		return toAjax(savepointBackupService.insertSavepointBackup(savepointBackup));
	}

	/**
	 * 修改savepoint备份地址
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		SavepointBackup savepointBackup = savepointBackupService.selectSavepointBackupById(id);
		mmap.put("savepointBackup", savepointBackup);
		return prefix + "/edit";
	}

	/**
	 * 修改保存savepoint备份地址
	 */
	@RequiresPermissions("flink:savepoint:edit")
	@Log(title = "savepoint备份地址", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(SavepointBackup savepointBackup) {
		return toAjax(savepointBackupService.updateSavepointBackup(savepointBackup));
	}

	/**
	 * 删除savepoint备份地址
	 */
	@RequiresPermissions("flink:savepoint:remove")
	@Log(title = "savepoint备份地址", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(savepointBackupService.deleteSavepointBackupByIds(ids));
	}
}
