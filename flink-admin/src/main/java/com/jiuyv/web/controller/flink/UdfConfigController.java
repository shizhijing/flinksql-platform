package com.jiuyv.web.controller.flink;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jiuyv.common.annotation.Log;
import com.jiuyv.common.enums.BusinessType;
import com.jiuyv.flink.domain.UdfConfig;
import com.jiuyv.flink.service.IUdfConfigService;
import com.jiuyv.framework.util.ShiroUtils;
import com.jiuyv.common.core.controller.BaseController;
import com.jiuyv.common.core.domain.AjaxResult;
import com.jiuyv.common.utils.poi.ExcelUtil;
import com.jiuyv.common.core.page.TableDataInfo;

/**
 * udf函数管理Controller
 * 
 * @author shu_k
 * @date 2021-04-01
 */
@Controller
@RequestMapping("/flink/udf")
public class UdfConfigController extends BaseController
{
    private String prefix = "flink/udf";

    @Autowired
    private IUdfConfigService udfConfigService;

    @RequiresPermissions("flink:udf:view")
    @GetMapping()
    public String udf()
    {
        return prefix + "/config";
    }

    /**
     * 查询udf函数管理列表
     */
    @RequiresPermissions("flink:udf:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UdfConfig udfConfig)
    {
        startPage();
        List<UdfConfig> list = udfConfigService.selectUdfConfigList(udfConfig);
        return getDataTable(list);
    }

    /**
     * 导出udf函数管理列表
     */
    @RequiresPermissions("flink:udf:export")
    @Log(title = "udf函数管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UdfConfig udfConfig)
    {
        List<UdfConfig> list = udfConfigService.selectUdfConfigList(udfConfig);
        ExcelUtil<UdfConfig> util = new ExcelUtil<UdfConfig>(UdfConfig.class);
        return util.exportExcel(list, "udf");
    }

    /**
     * 新增udf函数管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存udf函数管理
     */
    @RequiresPermissions("flink:udf:add")
    @Log(title = "udf函数管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(UdfConfig udfConfig)
    {
    	udfConfig.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(udfConfigService.insertUdfConfig(udfConfig));
    }

    /**
     * 修改udf函数管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        UdfConfig udfConfig = udfConfigService.selectUdfConfigById(id);
        mmap.put("udfConfig", udfConfig);
        return prefix + "/edit";
    }

    /**
     * 修改保存udf函数管理
     */
    @RequiresPermissions("flink:udf:edit")
    @Log(title = "udf函数管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UdfConfig udfConfig)
    {
        return toAjax(udfConfigService.updateUdfConfig(udfConfig));
    }

    /**
     * 删除udf函数管理
     */
    @RequiresPermissions("flink:udf:remove")
    @Log(title = "udf函数管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(udfConfigService.deleteUdfConfigByIds(ids));
    }
}
