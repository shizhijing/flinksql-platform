package com.jiuyv.web.controller.tool;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.jiuyv.common.core.controller.BaseController;

/**
 * @Description: swagger 接口
 * @author  shu_k
 * @date 2021年3月30日 下午3:45:28
 */
@Controller
@RequestMapping("/tool/swagger")
public class SwaggerController extends BaseController {
	@RequiresPermissions("tool:swagger:view")
	@GetMapping()
	public String index() {
		return redirect("/swagger-ui.html");
	}
}
