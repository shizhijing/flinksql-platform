package com.jiuyv.web.controller.flink;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jiuyv.common.annotation.Log;
import com.jiuyv.common.enums.BusinessType;
import com.jiuyv.flink.domain.IpStatus;
import com.jiuyv.flink.service.IIpStatusService;
import com.jiuyv.common.core.controller.BaseController;
import com.jiuyv.common.core.domain.AjaxResult;
import com.jiuyv.common.utils.poi.ExcelUtil;
import com.jiuyv.common.core.page.TableDataInfo;

/**
 * web服务运行ipController
 * 
 * @author shu_k
 * @date 2021-04-01
 */
@Controller
@RequestMapping("/flink/ipStatus")
public class IpStatusController extends BaseController {
	private String prefix = "flink/status";

	@Autowired
	private IIpStatusService ipStatusService;

	@RequiresPermissions("flink:status:view")
	@GetMapping()
	public String status() {
		return prefix + "/status";
	}

	/**
	 * 查询web服务运行ip列表
	 */
	@RequiresPermissions("flink:status:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(IpStatus ipStatus) {
		startPage();
		List<IpStatus> list = ipStatusService.selectIpStatusList(ipStatus);
		return getDataTable(list);
	}

	/**
	 * 导出web服务运行ip列表
	 */
	@RequiresPermissions("flink:status:export")
	@Log(title = "web服务运行ip", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(IpStatus ipStatus) {
		List<IpStatus> list = ipStatusService.selectIpStatusList(ipStatus);
		ExcelUtil<IpStatus> util = new ExcelUtil<IpStatus>(IpStatus.class);
		return util.exportExcel(list, "status");
	}

	/**
	 * 新增web服务运行ip
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存web服务运行ip
	 */
	@RequiresPermissions("flink:status:add")
	@Log(title = "web服务运行ip", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(IpStatus ipStatus) {
		return toAjax(ipStatusService.insertIpStatus(ipStatus));
	}

	/**
	 * 修改web服务运行ip
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		IpStatus ipStatus = ipStatusService.selectIpStatusById(id);
		mmap.put("ipStatus", ipStatus);
		return prefix + "/edit";
	}

	/**
	 * 修改保存web服务运行ip
	 */
	@RequiresPermissions("flink:status:edit")
	@Log(title = "web服务运行ip", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(IpStatus ipStatus) {
		return toAjax(ipStatusService.updateIpStatus(ipStatus));
	}

	/**
	 * 删除web服务运行ip
	 */
	@RequiresPermissions("flink:status:remove")
	@Log(title = "web服务运行ip", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(ipStatusService.deleteIpStatusByIds(ids));
	}
}
