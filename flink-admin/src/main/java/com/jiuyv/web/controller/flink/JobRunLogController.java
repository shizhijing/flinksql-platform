package com.jiuyv.web.controller.flink;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jiuyv.common.annotation.Log;
import com.jiuyv.common.enums.BusinessType;
import com.jiuyv.flink.domain.JobRunLog;
import com.jiuyv.flink.service.IJobRunLogService;
import com.jiuyv.common.core.controller.BaseController;
import com.jiuyv.common.core.domain.AjaxResult;
import com.jiuyv.common.utils.poi.ExcelUtil;
import com.jiuyv.common.core.page.TableDataInfo;

/**
 * 运行任务日志Controller
 * 
 * @author shu_k
 * @date 2021-04-01
 */
@Controller
@RequestMapping("/flink/log")
public class JobRunLogController extends BaseController {
	private String prefix = "flink/log";

	@Autowired
	private IJobRunLogService jobRunLogService;

	@RequiresPermissions("flink:log:view")
	@GetMapping()
	public String log() {
		return prefix + "/log";
	}

	/**
	 * 查询运行任务日志列表
	 */
	@RequiresPermissions("flink:log:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(JobRunLog jobRunLog) {
		startPage();
		List<JobRunLog> list = jobRunLogService.selectJobRunLogList(jobRunLog);
		return getDataTable(list);
	}

	/**
	 * 导出运行任务日志列表
	 */
	@RequiresPermissions("flink:log:export")
	@Log(title = "运行任务日志", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(JobRunLog jobRunLog) {
		List<JobRunLog> list = jobRunLogService.selectJobRunLogList(jobRunLog);
		ExcelUtil<JobRunLog> util = new ExcelUtil<JobRunLog>(JobRunLog.class);
		return util.exportExcel(list, "log");
	}

	/**
	 * 新增运行任务日志
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存运行任务日志
	 */
	@RequiresPermissions("flink:log:add")
	@Log(title = "运行任务日志", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(JobRunLog jobRunLog) {
		return toAjax(jobRunLogService.insertJobRunLog(jobRunLog));
	}

	/**
	 * 修改运行任务日志
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		JobRunLog jobRunLog = jobRunLogService.selectJobRunLogById(id);
		mmap.put("jobRunLog", jobRunLog);
		return prefix + "/edit";
	}

	/**
	 * 修改保存运行任务日志
	 */
	@RequiresPermissions("flink:log:edit")
	@Log(title = "运行任务日志", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(JobRunLog jobRunLog) {
		return toAjax(jobRunLogService.updateJobRunLog(jobRunLog));
	}

	/**
	 * 删除运行任务日志
	 */
	@RequiresPermissions("flink:log:remove")
	@Log(title = "运行任务日志", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(jobRunLogService.deleteJobRunLogByIds(ids));
	}
}
