package com.jiuyv.web.controller.flink;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jiuyv.common.annotation.Log;
import com.jiuyv.common.enums.BusinessType;
import com.jiuyv.flink.domain.AlartLog;
import com.jiuyv.flink.service.IAlartLogService;
import com.jiuyv.common.core.controller.BaseController;
import com.jiuyv.common.core.domain.AjaxResult;
import com.jiuyv.common.utils.poi.ExcelUtil;
import com.jiuyv.common.core.page.TableDataInfo;

/**
 * 告警发送情况日志Controller
 * 
 * @author shu_k
 * @date 2021-04-01
 */
@Controller
@RequestMapping("/flink/warningLog")
public class AlartLogController extends BaseController {
	private String prefix = "flink/warningLog";

	@Autowired
	private IAlartLogService alartLogService;

	@RequiresPermissions("flink:warningLog:view")
	@GetMapping()
	public String log() {
		return prefix + "/log";
	}

	/**
	 * 查询告警发送情况日志列表
	 */
	@RequiresPermissions("flink:warningLog:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(AlartLog alartLog) {
		startPage();
		List<AlartLog> list = alartLogService.selectAlartLogList(alartLog);
		return getDataTable(list);
	}

	/**
	 * 导出告警发送情况日志列表
	 */
	@RequiresPermissions("flink:warningLog:export")
	@Log(title = "告警发送情况日志", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(AlartLog alartLog) {
		List<AlartLog> list = alartLogService.selectAlartLogList(alartLog);
		ExcelUtil<AlartLog> util = new ExcelUtil<AlartLog>(AlartLog.class);
		return util.exportExcel(list, "log");
	}

	/**
	 * 新增告警发送情况日志
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存告警发送情况日志
	 */
	@RequiresPermissions("flink:warningLog:add")
	@Log(title = "告警发送情况日志", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(AlartLog alartLog) {
		return toAjax(alartLogService.insertAlartLog(alartLog));
	}

	/**
	 * 修改告警发送情况日志
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		AlartLog alartLog = alartLogService.selectAlartLogById(id);
		mmap.put("alartLog", alartLog);
		return prefix + "/edit";
	}

	/**
	 * 修改保存告警发送情况日志
	 */
	@RequiresPermissions("flink:warningLog:edit")
	@Log(title = "告警发送情况日志", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(AlartLog alartLog) {
		return toAjax(alartLogService.updateAlartLog(alartLog));
	}

	/**
	 * 删除告警发送情况日志
	 */
	@RequiresPermissions("flink:warningLog:remove")
	@Log(title = "告警发送情况日志", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(alartLogService.deleteAlartLogByIds(ids));
	}
}
