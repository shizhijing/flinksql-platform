package com.jiuyv.web.controller.flink;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jiuyv.common.annotation.Log;
import com.jiuyv.common.enums.BusinessType;
import com.jiuyv.flink.domain.JobConfig;
import com.jiuyv.flink.service.IJobConfigService;
import com.jiuyv.common.core.controller.BaseController;
import com.jiuyv.common.core.domain.AjaxResult;
import com.jiuyv.common.utils.poi.ExcelUtil;
import com.jiuyv.common.core.page.TableDataInfo;

/**
 * flink任务配置Controller
 * 
 * @author shu_k
 * @date 2021-04-01
 */
@Controller
@RequestMapping("/flink/config")
public class JobConfigController extends BaseController {
	private String prefix = "flink/config";

	@Autowired
	private IJobConfigService jobConfigService;

	@RequiresPermissions("flink:config:view")
	@GetMapping()
	public String config() {
		return prefix + "/config";
	}

	/**
	 * 查询flink任务配置列表
	 */
	@RequiresPermissions("flink:config:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(JobConfig jobConfig) {
		startPage();
		List<JobConfig> list = jobConfigService.selectJobConfigList(jobConfig);
		return getDataTable(list);
	}

	/**
	 * 导出flink任务配置列表
	 */
	@RequiresPermissions("flink:config:export")
	@Log(title = "flink任务配置", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(JobConfig jobConfig) {
		List<JobConfig> list = jobConfigService.selectJobConfigList(jobConfig);
		ExcelUtil<JobConfig> util = new ExcelUtil<JobConfig>(JobConfig.class);
		return util.exportExcel(list, "config");
	}

	/**
	 * 新增flink任务配置
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存flink任务配置
	 */
	@RequiresPermissions("flink:config:add")
	@Log(title = "flink任务配置", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(JobConfig jobConfig) {
		return toAjax(jobConfigService.insertJobConfig(jobConfig));
	}

	/**
	 * 修改flink任务配置
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		JobConfig jobConfig = jobConfigService.selectJobConfigById(id);
		mmap.put("jobConfig", jobConfig);
		return prefix + "/edit";
	}

	/**
	 * 修改保存flink任务配置
	 */
	@RequiresPermissions("flink:config:edit")
	@Log(title = "flink任务配置", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(JobConfig jobConfig) {
		return toAjax(jobConfigService.updateJobConfig(jobConfig));
	}

	/**
	 * 删除flink任务配置
	 */
	@RequiresPermissions("flink:config:remove")
	@Log(title = "flink任务配置", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(jobConfigService.deleteJobConfigByIds(ids));
	}
}
