package com.jiuyv.web.controller.flink;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jiuyv.common.annotation.Log;
import com.jiuyv.common.enums.BusinessType;
import com.jiuyv.flink.domain.SystemConfig;
import com.jiuyv.flink.service.ISystemConfigService;
import com.jiuyv.common.core.controller.BaseController;
import com.jiuyv.common.core.domain.AjaxResult;
import com.jiuyv.common.utils.poi.ExcelUtil;
import com.jiuyv.common.core.page.TableDataInfo;

/**
 * 系统配置Controller
 * 
 * @author shu_k
 * @date 2021-04-01
 */
@Controller
@RequestMapping("/flink/systemConfig")
public class SystemConfigController extends BaseController {
	private String prefix = "flink/systemConfig";

	@Autowired
	private ISystemConfigService systemConfigService;

	@RequiresPermissions("flink:systemConfig:view")
	@GetMapping()
	public String systemConfig() {
		return prefix + "/config";
	}

	/**
	 * 查询系统配置列表
	 */
	@RequiresPermissions("flink:systemConfig:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(SystemConfig systemConfig) {
		startPage();
		List<SystemConfig> list = systemConfigService.selectSystemConfigList(systemConfig);
		return getDataTable(list);
	}

	/**
	 * 导出系统配置列表
	 */
	@RequiresPermissions("flink:systemConfig:export")
	@Log(title = "系统配置", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(SystemConfig systemConfig) {
		List<SystemConfig> list = systemConfigService.selectSystemConfigList(systemConfig);
		ExcelUtil<SystemConfig> util = new ExcelUtil<SystemConfig>(SystemConfig.class);
		return util.exportExcel(list, "systemConfig");
	}

	/**
	 * 新增系统配置
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存系统配置
	 */
	@RequiresPermissions("flink:systemConfig:add")
	@Log(title = "系统配置", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(SystemConfig systemConfig) {
		return toAjax(systemConfigService.insertSystemConfig(systemConfig));
	}

	/**
	 * 修改系统配置
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		SystemConfig systemConfig = systemConfigService.selectSystemConfigById(id);
		mmap.put("systemConfig", systemConfig);
		return prefix + "/edit";
	}

	/**
	 * 修改保存系统配置
	 */
	@RequiresPermissions("flink:systemConfig:edit")
	@Log(title = "系统配置", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(SystemConfig systemConfig) {
		return toAjax(systemConfigService.updateSystemConfig(systemConfig));
	}

	/**
	 * 删除系统配置
	 */
	@RequiresPermissions("flink:systemConfig:remove")
	@Log(title = "系统配置", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(systemConfigService.deleteSystemConfigByIds(ids));
	}
}
