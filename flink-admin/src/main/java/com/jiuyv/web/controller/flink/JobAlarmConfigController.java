package com.jiuyv.web.controller.flink;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jiuyv.common.annotation.Log;
import com.jiuyv.common.enums.BusinessType;
import com.jiuyv.flink.domain.JobAlarmConfig;
import com.jiuyv.flink.service.IJobAlarmConfigService;
import com.jiuyv.common.core.controller.BaseController;
import com.jiuyv.common.core.domain.AjaxResult;
import com.jiuyv.common.utils.poi.ExcelUtil;
import com.jiuyv.common.core.page.TableDataInfo;

/**
 * 告警辅助配置Controller
 * 
 * @author shu_k
 * @date 2021-04-01
 */
@Controller
@RequestMapping("/flink/warning")
public class JobAlarmConfigController extends BaseController {
	private String prefix = "flink/warning";

	@Autowired
	private IJobAlarmConfigService jobAlarmConfigService;

	@RequiresPermissions("flink:config:view")
	@GetMapping()
	public String config() {
		return prefix + "/warning";
	}

	/**
	 * 查询告警辅助配置列表
	 */
	@RequiresPermissions("flink:warning:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(JobAlarmConfig jobAlarmConfig) {
		startPage();
		List<JobAlarmConfig> list = jobAlarmConfigService.selectJobAlarmConfigList(jobAlarmConfig);
		return getDataTable(list);
	}

	/**
	 * 导出告警辅助配置列表
	 */
	@RequiresPermissions("flink:warning:export")
	@Log(title = "告警辅助配置", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(JobAlarmConfig jobAlarmConfig) {
		List<JobAlarmConfig> list = jobAlarmConfigService.selectJobAlarmConfigList(jobAlarmConfig);
		ExcelUtil<JobAlarmConfig> util = new ExcelUtil<JobAlarmConfig>(JobAlarmConfig.class);
		return util.exportExcel(list, "config");
	}

	/**
	 * 新增告警辅助配置
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存告警辅助配置
	 */
	@RequiresPermissions("flink:warning:add")
	@Log(title = "告警辅助配置", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(JobAlarmConfig jobAlarmConfig) {
		return toAjax(jobAlarmConfigService.insertJobAlarmConfig(jobAlarmConfig));
	}

	/**
	 * 修改告警辅助配置
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		JobAlarmConfig jobAlarmConfig = jobAlarmConfigService.selectJobAlarmConfigById(id);
		mmap.put("jobAlarmConfig", jobAlarmConfig);
		return prefix + "/edit";
	}

	/**
	 * 修改保存告警辅助配置
	 */
	@RequiresPermissions("flink:warning:edit")
	@Log(title = "告警辅助配置", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(JobAlarmConfig jobAlarmConfig) {
		return toAjax(jobAlarmConfigService.updateJobAlarmConfig(jobAlarmConfig));
	}

	/**
	 * 删除告警辅助配置
	 */
	@RequiresPermissions("flink:warning:remove")
	@Log(title = "告警辅助配置", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(jobAlarmConfigService.deleteJobAlarmConfigByIds(ids));
	}
}
