package com.jiuyv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @Description: 启动程序
 * @author shu_k
 * @date 2021年3月30日 下午3:28:38
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class RuoYiApplication {
	public static void main(String[] args) {
		SpringApplication.run(RuoYiApplication.class, args);
	}
}