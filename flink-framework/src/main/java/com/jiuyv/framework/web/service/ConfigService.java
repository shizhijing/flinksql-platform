package com.jiuyv.framework.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiuyv.system.service.ISysConfigService;

/**
 * @Description: 首创 html调用 thymeleaf 实现参数管理
 * @author  shu_k
 * @date 2021年3月30日 下午2:44:38
 */
@Service("config")
public class ConfigService {
	@Autowired
	private ISysConfigService configService;

	/**
	 * 根据键名查询参数配置信息
	 * 
	 * @param configKey 参数键名
	 * @return 参数键值
	 */
	public String getKey(String configKey) {
		return configService.selectConfigByKey(configKey);
	}
}
