package com.jiuyv.framework.web.domain.server;

import com.jiuyv.common.utils.Arith;

/**
 * @Description: 內存相关信息
 * @author  shu_k
 * @date 2021年3月30日 下午2:43:30
 */
public class Mem {
	/**
	 * 内存总量
	 */
	private double total;

	/**
	 * 已用内存
	 */
	private double used;

	/**
	 * 剩余内存
	 */
	private double free;

	public double getTotal() {
		return Arith.div(total, (1024 * 1024 * 1024), 2);
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public double getUsed() {
		return Arith.div(used, (1024 * 1024 * 1024), 2);
	}

	public void setUsed(long used) {
		this.used = used;
	}

	public double getFree() {
		return Arith.div(free, (1024 * 1024 * 1024), 2);
	}

	public void setFree(long free) {
		this.free = free;
	}

	public double getUsage() {
		return Arith.mul(Arith.div(used, total, 4), 100);
	}
}
