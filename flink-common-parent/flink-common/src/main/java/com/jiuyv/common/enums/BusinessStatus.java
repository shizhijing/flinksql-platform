package com.jiuyv.common.enums;

/**
 * @Description: 操作状态
 * @author  shu_k
 * @date 2021年3月30日 上午10:30:59
 */
public enum BusinessStatus {
	/**
	 * 成功
	 */
	SUCCESS,

	/**
	 * 失败
	 */
	FAIL,
}
