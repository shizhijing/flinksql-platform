package com.jiuyv.common.exception.user;

/**
 * 
 * @Description: 验证码错误异常类
 * @author  shu_k
 * @date 2021年3月30日 上午10:38:17
 */
public class CaptchaException extends UserException {
	private static final long serialVersionUID = 1L;

	public CaptchaException() {
		super("user.jcaptcha.error", null);
	}
}
