package com.jiuyv.common.exception.file;

/**
 * 
 * @Description: 文件名称超长限制异常类 
 * @author  shu_k
 * @date 2021年3月30日 上午10:37:01
 */
public class FileNameLengthLimitExceededException extends FileException {
	private static final long serialVersionUID = 1L;

	public FileNameLengthLimitExceededException(int defaultFileNameLength) {
		super("upload.filename.exceed.length", new Object[] { defaultFileNameLength });
	}
}
