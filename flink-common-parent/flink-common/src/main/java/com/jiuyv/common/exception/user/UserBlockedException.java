package com.jiuyv.common.exception.user;

/**
 * @Description: 用户锁定异常类
 * @author  shu_k
 * @date 2021年3月30日 上午10:39:18
 */
public class UserBlockedException extends UserException {
	private static final long serialVersionUID = 1L;

	public UserBlockedException() {
		super("user.blocked", null);
	}
}
