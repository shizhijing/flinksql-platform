package com.jiuyv.common.exception.file;

import com.jiuyv.common.exception.base.BaseException;

/**
 * 
 * @Description: 文件信息异常类
 * @author  shu_k
 * @date 2021年3月30日 上午10:36:40
 */
public class FileException extends BaseException {
	private static final long serialVersionUID = 1L;

	public FileException(String code, Object[] args) {
		super("file", code, args, null);
	}

}
