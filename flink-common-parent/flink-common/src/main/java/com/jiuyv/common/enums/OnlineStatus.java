package com.jiuyv.common.enums;

/**
 * 
 * @Description: 用户会话
 * @author  shu_k
 * @date 2021年3月30日 上午10:31:41
 */
public enum OnlineStatus {
	/** 用户状态 */
	on_line("在线"), off_line("离线");

	private final String info;

	private OnlineStatus(String info) {
		this.info = info;
	}

	public String getInfo() {
		return info;
	}
}
