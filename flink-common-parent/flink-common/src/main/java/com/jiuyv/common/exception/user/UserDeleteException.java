package com.jiuyv.common.exception.user;

/**
 * 
 * @Description: 用户账号已被删除
 * @author  shu_k
 * @date 2021年3月30日 上午10:39:39
 */
public class UserDeleteException extends UserException {
	private static final long serialVersionUID = 1L;

	public UserDeleteException() {
		super("user.password.delete", null);
	}
}
