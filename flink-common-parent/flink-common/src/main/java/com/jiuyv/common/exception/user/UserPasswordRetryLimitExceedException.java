package com.jiuyv.common.exception.user;

/**
 * @Description: 用户错误最大次数异常类
 * @author  shu_k
 * @date 2021年3月30日 上午10:40:29
 */
public class UserPasswordRetryLimitExceedException extends UserException {
	private static final long serialVersionUID = 1L;

	public UserPasswordRetryLimitExceedException(int retryLimitCount) {
		super("user.password.retry.limit.exceed", new Object[] { retryLimitCount });
	}
}
