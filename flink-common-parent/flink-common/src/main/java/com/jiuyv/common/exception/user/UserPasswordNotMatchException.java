package com.jiuyv.common.exception.user;

/**
 * 
 * @Description: 用户密码不正确或不符合规范异常类
 * @author  shu_k
 * @date 2021年3月30日 上午10:40:06
 */
public class UserPasswordNotMatchException extends UserException {
	private static final long serialVersionUID = 1L;

	public UserPasswordNotMatchException() {
		super("user.password.not.match", null);
	}
}
