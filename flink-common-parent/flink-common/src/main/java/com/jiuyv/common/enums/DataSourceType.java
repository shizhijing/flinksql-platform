package com.jiuyv.common.enums;

/**
 * 
 * @Description: 数据源
 * @author  shu_k
 * @date 2021年3月30日 上午10:31:25
 */
public enum DataSourceType {
	/**
	 * 主库
	 */
	MASTER,

	/**
	 * 从库
	 */
	SLAVE
}
