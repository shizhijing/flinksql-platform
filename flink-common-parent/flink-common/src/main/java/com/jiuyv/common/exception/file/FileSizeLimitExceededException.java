package com.jiuyv.common.exception.file;

/**
 * @Description: 文件名大小限制异常类
 * @author  shu_k
 * @date 2021年3月31日 上午11:10:29
 */
public class FileSizeLimitExceededException extends FileException {
	
	private static final long serialVersionUID = 1L;

	public FileSizeLimitExceededException(long defaultMaxSize) {
		super("upload.exceed.maxSize", new Object[] { defaultMaxSize });
	}
}
