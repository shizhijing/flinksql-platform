package com.jiuyv.common.exception.user;

/**
 * 
 * @Description: 用户不存在异常类
 * @author  shu_k
 * @date 2021年3月30日 上午10:39:52
 */
public class UserNotExistsException extends UserException {
	private static final long serialVersionUID = 1L;

	public UserNotExistsException() {
		super("user.not.exists", null);
	}
}
