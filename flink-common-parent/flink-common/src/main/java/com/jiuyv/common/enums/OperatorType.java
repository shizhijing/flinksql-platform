package com.jiuyv.common.enums;

/**
 * 
 * @Description: 操作人类别
 * @author  shu_k
 * @date 2021年3月30日 上午10:32:03
 */
public enum OperatorType {
	/**
	 * 其它
	 */
	OTHER,

	/**
	 * 后台用户
	 */
	MANAGE,

	/**
	 * 手机端用户
	 */
	MOBILE
}
