package com.jiuyv.common.exception;

/**
 * 
 * @Description: 业务异常
 * @author  shu_k
 * @date 2021年3月30日 上午10:32:45
 */
public class BusinessException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	protected final String message;

	public BusinessException(String message) {
		this.message = message;
	}

	public BusinessException(String message, Throwable e) {
		super(message, e);
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
