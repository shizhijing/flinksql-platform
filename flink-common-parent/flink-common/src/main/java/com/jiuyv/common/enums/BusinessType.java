package com.jiuyv.common.enums;

/**
 * 
 * @Description: 业务操作类型
 * @author  shu_k
 * @date 2021年3月30日 上午10:31:13
 */
public enum BusinessType {
	/**
	 * 其它
	 */
	OTHER,

	/**
	 * 新增
	 */
	INSERT,

	/**
	 * 修改
	 */
	UPDATE,

	/**
	 * 删除
	 */
	DELETE,

	/**
	 * 授权
	 */
	GRANT,

	/**
	 * 导出
	 */
	EXPORT,

	/**
	 * 导入
	 */
	IMPORT,

	/**
	 * 强退
	 */
	FORCE,

	/**
	 * 生成代码
	 */
	GENCODE,

	/**
	 * 清空
	 */
	CLEAN,
}
