package com.jiuyv.common.constant;

/**
 * @Description: 权限通用常量
 * @author shu_k
 * @date 2021年3月30日 上午10:23:51
 */
public class PermissionConstants {
	/** 新增权限 */
	public static final String ADD_PERMISSION = "add";

	/** 修改权限 */
	public static final String EDIT_PERMISSION = "edit";

	/** 删除权限 */
	public static final String REMOVE_PERMISSION = "remove";

	/** 导出权限 */
	public static final String EXPORT_PERMISSION = "export";

	/** 显示权限 */
	public static final String VIEW_PERMISSION = "view";

	/** 查询权限 */
	public static final String LIST_PERMISSION = "list";
}
