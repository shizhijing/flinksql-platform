package com.jiuyv.common.exception.user;

/**
 * 
 * @Description: 用户错误记数异常类
 * @author  shu_k
 * @date 2021年3月30日 上午10:40:18
 */
public class UserPasswordRetryLimitCountException extends UserException {
	private static final long serialVersionUID = 1L;

	public UserPasswordRetryLimitCountException(int retryLimitCount) {
		super("user.password.retry.limit.count", new Object[] { retryLimitCount });
	}
}
