package com.jiuyv.common.utils.security;

import java.security.MessageDigest;

import org.apache.commons.codec.Charsets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @Description: Md5加密方法
 * @author  shu_k
 * @date 2021年3月30日 下午1:07:10
 */
public class Md5Utils {
	
	private static final Logger log = LoggerFactory.getLogger(Md5Utils.class);

	private static byte[] md5(String s) {
		MessageDigest algorithm;
		try {
			algorithm = MessageDigest.getInstance("MD5");
			algorithm.reset();
			algorithm.update(s.getBytes(Charsets.UTF_8));
			byte[] messageDigest = algorithm.digest();
			return messageDigest;
		} catch (Exception e) {
			log.error("MD5 Error...", e);
			
		}
		return new byte[0];
	}

	private static final String toHex(byte hash[]) {
		if (hash == null) {
			return null;
		}
		StringBuilder buf = new StringBuilder(hash.length * 2);
		int i;

		for (i = 0; i < hash.length; i++) {
			if ((hash[i] & 0xff) < 0x10) {
				buf.append("0");
			}
			buf.append(Long.toString(hash[i] & 0xff, 16));
		}
		return buf.toString();
	}

	public static String hash(String s) {
		try {
			String hex = toHex(md5(s));
			if(null!=hex) {
				return new String(hex.getBytes(Charsets.UTF_8), Charsets.UTF_8);
			}else {
				return null;
			}
			
		} catch (Exception e) {
			log.error("not supported charset {}", e.getMessage());
			return s;
		}
	}
}
