package com.jiuyv.common.exception.user;

import com.jiuyv.common.exception.base.BaseException;

/**
 * @Description: 用户信息异常类
 * @author  shu_k
 * @date 2021年3月30日 上午10:39:06
 */
public class UserException extends BaseException {
	
	private static final long serialVersionUID = 1L;

	public UserException(String code, Object[] args) {
		super("user", code, args, null);
	}
}
