package com.jiuyv.common.exception.user;

/**
 * 
 * @Description: 角色锁定异常类
 * @author  shu_k
 * @date 2021年3月30日 上午10:38:41
 */
public class RoleBlockedException extends UserException {
	private static final long serialVersionUID = 1L;

	public RoleBlockedException() {
		super("role.blocked", null);
	}
}
