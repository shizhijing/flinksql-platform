package com.jiuyv.common.exception;

/**
 * 
 * @Description: 演示模式异常
 * @author  shu_k
 * @date 2021年3月30日 上午10:32:56
 */
public class DemoModeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public DemoModeException() {
	}
}
