package com.jiuyv.flink.streaming.common.model;

import com.jiuyv.flink.streaming.common.enums.SqlCommand;

/**
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author  shu_k
 * @date 2021年3月31日 下午4:56:15
 */

public class SqlCommandCall {

    public SqlCommand sqlCommand;

    public String[] operands;
    
    public SqlCommand getSqlCommand() {
		return sqlCommand;
	}

	public void setSqlCommand(SqlCommand sqlCommand) {
		this.sqlCommand = sqlCommand;
	}

	public String[] getOperands() {
		return operands;
	}

	public void setOperands(String[] operands) {
		this.operands = operands;
	}

	public SqlCommandCall(SqlCommand sqlCommand, String[] operands) {
        this.sqlCommand = sqlCommand;
        this.operands = operands;
    }

    public SqlCommandCall(String[] operands) {
        this.operands = operands;
    }
}
