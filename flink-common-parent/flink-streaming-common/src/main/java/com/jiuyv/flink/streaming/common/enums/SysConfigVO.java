package com.jiuyv.flink.streaming.common.enums;


/**
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author  shu_k
 * @date 2021年4月1日 上午11:14:53
 */
public class SysConfigVO {

    private String key;

    private String desc;

    public SysConfigVO(String key, String desc) {
        this.key = key;
        this.desc = desc;
    }

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
    
}
