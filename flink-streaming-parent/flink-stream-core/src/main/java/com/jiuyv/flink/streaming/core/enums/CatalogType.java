package com.jiuyv.flink.streaming.core.enums;


/**
 * @author Jim Chen
 * @date 2021-01-21
 * @time 01:18
 */
public enum CatalogType {

    MEMORY,

    HIVE,

    JDBC,

    POSTGRES
    
}
