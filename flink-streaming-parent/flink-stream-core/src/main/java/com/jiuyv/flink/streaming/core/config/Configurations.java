package com.jiuyv.flink.streaming.core.config;

import org.apache.commons.lang3.StringUtils;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.table.api.TableEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author  shu_k
 * @date 2021年4月1日 上午9:48:52
 */
public class Configurations {
	
	private static final Logger log = LoggerFactory.getLogger(Configurations.class);


    /**
     * 单个设置Configuration
     *
     * @author zhuhuipei
     * @date 2021/3/23
     * @time 23:58
     */
    public static void setSingleConfiguration(TableEnvironment tEnv, String key, String value) {
        if (StringUtils.isEmpty(key) || StringUtils.isEmpty(value)) {
            return;
        }
        Configuration configuration = tEnv.getConfig().getConfiguration();
        log.info("#############setConfiguration#############\n  key={} value={}", key, value);
        configuration.setString(key, value);

    }


}
