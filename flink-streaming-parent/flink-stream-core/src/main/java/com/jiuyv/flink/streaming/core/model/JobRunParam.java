package com.jiuyv.flink.streaming.core.model;

import com.jiuyv.flink.streaming.common.model.CheckPointParam;
/**
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author  shu_k
 * @date 2021年3月31日 下午6:18:41
 */

public class JobRunParam {
    /**
     * sql语句目录
     */
    private String sqlPath;


    /**
     * CheckPoint 参数
     */
    private CheckPointParam checkPointParam;


	public String getSqlPath() {
		return sqlPath;
	}


	public void setSqlPath(String sqlPath) {
		this.sqlPath = sqlPath;
	}


	public CheckPointParam getCheckPointParam() {
		return checkPointParam;
	}


	public void setCheckPointParam(CheckPointParam checkPointParam) {
		this.checkPointParam = checkPointParam;
	}
    

}
