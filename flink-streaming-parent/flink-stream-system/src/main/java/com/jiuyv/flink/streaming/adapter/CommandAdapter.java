package com.jiuyv.flink.streaming.adapter;

import com.jiuyv.flink.streaming.enums.DeployModeEnum;

/**
 * @author shu_k
 * @Description:
 * @date 2020-09-18
 * @time 20:09
 */
public interface CommandAdapter {


    /**
     * 启动服务
     *
     * @author shu_k
     * @date 2021/3/26
     * @time 17:31
     */
    String submitJob(String command, StringBuilder localLog, Long jobRunLogId, DeployModeEnum deployModeEnum) throws Exception;


    /**
     * yarn per模式执行savepoint
     * <p>
     * 	默认savepoint保存的地址是：hdfs:///flink/savepoint/flink-streaming-platform-web/
     *
     * @author shu_k
     * @date 2020-09-21
     * @time 23:14
     */
    void savepointForPerYarn(String jobId, String targetDirectory, String yarnAppId) throws Exception;


}
