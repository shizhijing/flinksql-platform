package com.jiuyv.flink.streaming.dto;

import org.springframework.util.CollectionUtils;

import com.jiuyv.flink.domain.JobConfig;
import com.jiuyv.flink.streaming.enums.AlarmTypeEnum;
import com.jiuyv.flink.streaming.enums.DeployModeEnum;
import com.jiuyv.flink.streaming.enums.JobConfigStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author zhuhuipei
 * @date 2020-07-10
 * @time 01:46
 */
public class JobConfigDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 任务名称
     */
    private String jobName;

    /**
     * flink的模式
     */
    private DeployModeEnum deployModeEnum;

    /**
     * flink运行配置
     */
    private String flinkRunConfig;

    /**
     * checkpointConfig 配置
     */
    private String flinkCheckpointConfig;

    /**
     * flink运行配置
     */
    private String jobId;

    /**
     * 1:开启 0: 关闭
     */
    private Integer isOpen;

    /**
     * @see com.flink.streaming.web.enums.JobConfigStatus
     * 1:运行中 0: 停止中 -1:运行失败
     */
    private JobConfigStatus status;


    /**
     * 三方jar udf、 连接器 等jar如http://xxx.xxx.com/flink-streaming-udf.jar
     */
    private String extJarPath;

    /**
     * 最后一次启动时间
     */
    private Date lastStartTime;

    /**
     * 更新版本号 用于乐观锁
     */
    private Integer version;

    /**
     * sql语句
     */
    private String flinkSql;

    private List<AlarmTypeEnum> alarmTypeEnumList;


    private Long lastRunLogId;


    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    private String createBy;

    private String updateBy;

    public static JobConfig toEntity(JobConfigDTO jobConfigDTO) {
        if (jobConfigDTO == null) {
            return null;
        }
        JobConfig jobConfig = new JobConfig();
        jobConfig.setId(jobConfigDTO.getId());
        jobConfig.setJobName(jobConfigDTO.getJobName());
        if (jobConfigDTO.getDeployModeEnum() != null) {
            jobConfig.setDeployMode(jobConfigDTO.getDeployModeEnum().name());
        }
        jobConfig.setFlinkRunConfig(jobConfigDTO.getFlinkRunConfig());
        jobConfig.setFlinkCheckpointConfig(jobConfigDTO.getFlinkCheckpointConfig());
        jobConfig.setJobId(jobConfigDTO.getJobId());
        jobConfig.setIsOpen(jobConfigDTO.getIsOpen());
        jobConfig.setStatus(jobConfigDTO.getStatus().getCode());
        jobConfig.setLastStartTime(jobConfigDTO.getLastStartTime());
        jobConfig.setVersion(jobConfigDTO.getVersion().longValue());
        jobConfig.setFlinkSql(jobConfigDTO.getFlinkSql());
        jobConfig.setCreateTime(jobConfigDTO.getCreateTime());
        jobConfig.setUpdateTime(jobConfigDTO.getUpdateTime());
        jobConfig.setCreateBy(jobConfigDTO.getCreateBy());
        jobConfig.setUpdateBy(jobConfigDTO.getUpdateBy());
        jobConfig.setLastRunLogId(jobConfigDTO.getLastRunLogId());
        jobConfig.setExtJarPath(jobConfigDTO.getExtJarPath());
        return jobConfig;
    }


    public static JobConfigDTO toDTO(JobConfig jobConfig) {
        if (jobConfig == null) {
            return null;
        }
        JobConfigDTO jobConfigDTO = new JobConfigDTO();
        jobConfigDTO.setId(jobConfig.getId());
        jobConfigDTO.setJobName(jobConfig.getJobName());
        jobConfigDTO.setDeployModeEnum(DeployModeEnum.getModel(jobConfig.getDeployMode()));
        jobConfigDTO.setFlinkRunConfig(jobConfig.getFlinkRunConfig());
        jobConfigDTO.setFlinkCheckpointConfig(jobConfig.getFlinkCheckpointConfig());
        jobConfigDTO.setJobId(jobConfig.getJobId());
        jobConfigDTO.setIsOpen(jobConfig.getIsOpen());
        jobConfigDTO.setStatus(JobConfigStatus.getJobConfigStatus(jobConfig.getStatus()));
        jobConfigDTO.setLastStartTime(jobConfig.getLastStartTime());
        jobConfigDTO.setVersion(jobConfig.getVersion().intValue());
        jobConfigDTO.setCreateTime(jobConfig.getCreateTime());
        jobConfigDTO.setUpdateTime(jobConfig.getUpdateTime());
        jobConfigDTO.setCreateBy(jobConfig.getCreateBy());
        jobConfigDTO.setUpdateBy(jobConfig.getUpdateBy());
        jobConfigDTO.setFlinkSql(jobConfig.getFlinkSql());
        jobConfigDTO.setLastRunLogId(jobConfig.getLastRunLogId());
        jobConfigDTO.setExtJarPath(jobConfig.getExtJarPath());

        return jobConfigDTO;
    }


    public static List<JobConfigDTO> toListDTO(List<JobConfig> jobConfigList) {
        if (CollectionUtils.isEmpty(jobConfigList)) {
            return Collections.emptyList();
        }

        List<JobConfigDTO> jobConfigDTOList = new ArrayList<JobConfigDTO>();

        for (JobConfig jobConfig : jobConfigList) {
            jobConfigDTOList.add(toDTO(jobConfig));
        }

        return jobConfigDTOList;
    }


    public static String buildRunName(String jobName) {

        return "flink@" + jobName;
    }


    public static JobConfigDTO bulidStop(Long id) {
        JobConfigDTO jobConfig = new JobConfigDTO();
        jobConfig.setStatus(JobConfigStatus.STOP);
        jobConfig.setUpdateBy("sys_auto");
        jobConfig.setId(id);
        jobConfig.setJobId("");
        return jobConfig;
    }


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public DeployModeEnum getDeployModeEnum() {
		return deployModeEnum;
	}

	public void setDeployModeEnum(DeployModeEnum deployModeEnum) {
		this.deployModeEnum = deployModeEnum;
	}

	public String getFlinkRunConfig() {
		return flinkRunConfig;
	}

	public void setFlinkRunConfig(String flinkRunConfig) {
		this.flinkRunConfig = flinkRunConfig;
	}

	public String getFlinkCheckpointConfig() {
		return flinkCheckpointConfig;
	}

	public void setFlinkCheckpointConfig(String flinkCheckpointConfig) {
		this.flinkCheckpointConfig = flinkCheckpointConfig;
	}


	public String getJobId() {
		return jobId;
	}


	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public Integer getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(Integer isOpen) {
		this.isOpen = isOpen;
	}

	public JobConfigStatus getStatus() {
		return status;
	}

	public void setStatus(JobConfigStatus status) {
		this.status = status;
	}

	public String getExtJarPath() {
		return extJarPath;
	}

	public void setExtJarPath(String extJarPath) {
		this.extJarPath = extJarPath;
	}

	public Date getLastStartTime() {
		return lastStartTime;
	}

	public void setLastStartTime(Date lastStartTime) {
		this.lastStartTime = lastStartTime;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getFlinkSql() {
		return flinkSql;
	}

	public void setFlinkSql(String flinkSql) {
		this.flinkSql = flinkSql;
	}

	public List<AlarmTypeEnum> getAlarmTypeEnumList() {
		return alarmTypeEnumList;
	}

	public void setAlarmTypeEnumList(List<AlarmTypeEnum> alarmTypeEnumList) {
		this.alarmTypeEnumList = alarmTypeEnumList;
	}

	public Long getLastRunLogId() {
		return lastRunLogId;
	}

	public void setLastRunLogId(Long lastRunLogId) {
		this.lastRunLogId = lastRunLogId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}


	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

    
}
