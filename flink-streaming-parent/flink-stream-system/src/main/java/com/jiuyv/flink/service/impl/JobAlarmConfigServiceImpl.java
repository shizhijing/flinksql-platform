package com.jiuyv.flink.service.impl;

import java.util.List;
import com.jiuyv.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiuyv.flink.mapper.JobAlarmConfigMapper;
import com.jiuyv.flink.domain.JobAlarmConfig;
import com.jiuyv.flink.service.IJobAlarmConfigService;
import com.jiuyv.flink.streaming.enums.AlarmTypeEnum;
import com.jiuyv.common.core.text.Convert;

/**
 * 告警辅助配置Service业务层处理
 * 
 * @author shu_k
 * @date 2021-04-01
 */
@Service
public class JobAlarmConfigServiceImpl implements IJobAlarmConfigService 
{
    @Autowired
    private JobAlarmConfigMapper jobAlarmConfigMapper;

    /**
     * 查询告警辅助配置
     * 
     * @param id 告警辅助配置ID
     * @return 告警辅助配置
     */
    @Override
    public JobAlarmConfig selectJobAlarmConfigById(Long id)
    {
        return jobAlarmConfigMapper.selectJobAlarmConfigById(id);
    }

    /**
     * 查询告警辅助配置列表
     * 
     * @param jobAlarmConfig 告警辅助配置
     * @return 告警辅助配置
     */
    @Override
    public List<JobAlarmConfig> selectJobAlarmConfigList(JobAlarmConfig jobAlarmConfig)
    {
        return jobAlarmConfigMapper.selectJobAlarmConfigList(jobAlarmConfig);
    }

    /**
     * 新增告警辅助配置
     * 
     * @param jobAlarmConfig 告警辅助配置
     * @return 结果
     */
    @Override
    public int insertJobAlarmConfig(JobAlarmConfig jobAlarmConfig)
    {
        jobAlarmConfig.setCreateTime(DateUtils.getNowDate());
        return jobAlarmConfigMapper.insertJobAlarmConfig(jobAlarmConfig);
    }

    /**
     * 修改告警辅助配置
     * 
     * @param jobAlarmConfig 告警辅助配置
     * @return 结果
     */
    @Override
    public int updateJobAlarmConfig(JobAlarmConfig jobAlarmConfig)
    {
        jobAlarmConfig.setUpdateTime(DateUtils.getNowDate());
        return jobAlarmConfigMapper.updateJobAlarmConfig(jobAlarmConfig);
    }

    /**
     * 删除告警辅助配置对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteJobAlarmConfigByIds(String ids)
    {
        return jobAlarmConfigMapper.deleteJobAlarmConfigByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除告警辅助配置信息
     * 
     * @param id 告警辅助配置ID
     * @return 结果
     */
    @Override
    public int deleteJobAlarmConfigById(Long id)
    {
        return jobAlarmConfigMapper.deleteJobAlarmConfigById(id);
    }

	@Override
	public void upSertBatchJobAlarmConfig(List<AlarmTypeEnum> alarmTypeEnumList, Long jobConfigId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<AlarmTypeEnum> findByJobId(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
}
