package com.jiuyv.flink.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.jiuyv.common.annotation.Excel;
import com.jiuyv.common.core.domain.BaseEntity;

/**
 * flink任务配置对象 flink_job_config
 * 
 * @author shu_k
 * @date 2021-04-01
 */
public class JobConfig extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** $column.columnComment */
	private Long id;

	/** 任务名称 */
	@Excel(name = "任务名称")
	private String jobName;

	/** 提交模式: standalone 、yarn 、yarn-session */
	@Excel(name = "提交模式: standalone 、yarn 、yarn-session ")
	private String deployMode;

	/** flink运行配置 */
	@Excel(name = "flink运行配置")
	private String flinkRunConfig;

	/** sql语句 */
	@Excel(name = "sql语句")
	private String flinkSql;

	/** checkPoint配置 */
	@Excel(name = "checkPoint配置")
	private String flinkCheckpointConfig;

	/** 运行后的任务id */
	@Excel(name = "运行后的任务id")
	private String jobId;

	/** 1:开启 0: 关闭 */
	@Excel(name = "1:开启 0: 关闭")
	private Integer isOpen;

	/** 1:运行中 0: 停止中 -1:运行失败 */
	@Excel(name = "1:运行中 0: 停止中 -1:运行失败")
	private Integer status;

	/** udf地址已经连接器jar 如http://xxx.xxx.com/flink-streaming-udf.jar */
	@Excel(name = "udf地址已经连接器jar 如http://xxx.xxx.com/flink-streaming-udf.jar")
	private String extJarPath;

	/** 最后一次启动时间 */
	@Excel(name = "最后一次启动时间", width = 30, dateFormat = "yyyy-MM-dd")
	private Date lastStartTime;

	/** 最后一次日志 */
	@Excel(name = "最后一次日志")
	private Long lastRunLogId;

	/** 更新版本号 用于乐观锁 */
	@Excel(name = "更新版本号 用于乐观锁")
	private Long version;

	/** $column.columnComment */
	@Excel(name = "更新版本号 用于乐观锁")
	private Integer isDeleted;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setDeployMode(String deployMode) {
		this.deployMode = deployMode;
	}

	public String getDeployMode() {
		return deployMode;
	}

	public void setFlinkRunConfig(String flinkRunConfig) {
		this.flinkRunConfig = flinkRunConfig;
	}

	public String getFlinkRunConfig() {
		return flinkRunConfig;
	}

	public void setFlinkSql(String flinkSql) {
		this.flinkSql = flinkSql;
	}

	public String getFlinkSql() {
		return flinkSql;
	}

	public void setFlinkCheckpointConfig(String flinkCheckpointConfig) {
		this.flinkCheckpointConfig = flinkCheckpointConfig;
	}

	public String getFlinkCheckpointConfig() {
		return flinkCheckpointConfig;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getJobId() {
		return jobId;
	}

	public void setIsOpen(Integer isOpen) {
		this.isOpen = isOpen;
	}

	public Integer getIsOpen() {
		return isOpen;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setExtJarPath(String extJarPath) {
		this.extJarPath = extJarPath;
	}

	public String getExtJarPath() {
		return extJarPath;
	}

	public void setLastStartTime(Date lastStartTime) {
		this.lastStartTime = lastStartTime;
	}

	public Date getLastStartTime() {
		return lastStartTime;
	}

	public void setLastRunLogId(Long lastRunLogId) {
		this.lastRunLogId = lastRunLogId;
	}

	public Long getLastRunLogId() {
		return lastRunLogId;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getVersion() {
		return version;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("id", getId())
				.append("jobName", getJobName()).append("deployMode", getDeployMode())
				.append("flinkRunConfig", getFlinkRunConfig()).append("flinkSql", getFlinkSql())
				.append("flinkCheckpointConfig", getFlinkCheckpointConfig()).append("jobId", getJobId())
				.append("isOpen", getIsOpen()).append("status", getStatus()).append("extJarPath", getExtJarPath())
				.append("lastStartTime", getLastStartTime()).append("lastRunLogId", getLastRunLogId())
				.append("version", getVersion()).append("isDeleted", getIsDeleted())
				.append("createTime", getCreateTime()).append("updateTime", getUpdateTime())
				.append("createBy", getCreateBy()).append("updateBy", getUpdateBy()).toString();
	}
}
