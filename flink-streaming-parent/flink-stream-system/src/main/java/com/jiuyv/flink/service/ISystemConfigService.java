package com.jiuyv.flink.service;

import java.util.List;
import com.jiuyv.flink.domain.SystemConfig;
import com.jiuyv.flink.streaming.common.enums.SysConfigEnumType;
import com.jiuyv.flink.streaming.dto.SystemConfigDTO;
import com.jiuyv.flink.streaming.enums.DeployModeEnum;

/**
 * 系统配置Service接口
 * 
 * @author shu_k
 * @date 2021-04-01
 */
public interface ISystemConfigService 
{
    /**
     * 查询系统配置
     * 
     * @param id 系统配置ID
     * @return 系统配置
     */
    public SystemConfig selectSystemConfigById(Long id);

    /**
     * 查询系统配置列表
     * 
     * @param systemConfig 系统配置
     * @return 系统配置集合
     */
    public List<SystemConfig> selectSystemConfigList(SystemConfig systemConfig);

    /**
     * 新增系统配置
     * 
     * @param systemConfig 系统配置
     * @return 结果
     */
    public int insertSystemConfig(SystemConfig systemConfig);

    /**
     * 修改系统配置
     * 
     * @param systemConfig 系统配置
     * @return 结果
     */
    public int updateSystemConfig(SystemConfig systemConfig);

    /**
     * 批量删除系统配置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSystemConfigByIds(String ids);

    /**
     * 删除系统配置信息
     * 
     * @param id 系统配置ID
     * @return 结果
     */
    public int deleteSystemConfigById(Long id);

	public String getSystemConfigByKey(String key);

	public int getYarnRmHttpAddress();

	public String getFlinkHttpAddress(DeployModeEnum deployModeEnum);

	public List<SystemConfigDTO> getSystemConfig(SysConfigEnumType sys);
}
