package com.jiuyv.flink.service.impl;

import java.util.List;
import com.jiuyv.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiuyv.flink.mapper.JobRunLogMapper;
import com.jiuyv.flink.domain.JobRunLog;
import com.jiuyv.flink.service.IJobRunLogService;
import com.jiuyv.flink.streaming.dto.JobRunLogDTO;
import com.jiuyv.common.core.text.Convert;

/**
 * 运行任务日志Service业务层处理
 * 
 * @author shu_k
 * @date 2021-04-01
 */
@Service
public class JobRunLogServiceImpl implements IJobRunLogService 
{
    @Autowired
    private JobRunLogMapper jobRunLogMapper;

    /**
     * 查询运行任务日志
     * 
     * @param id 运行任务日志ID
     * @return 运行任务日志
     */
    @Override
    public JobRunLog selectJobRunLogById(Long id)
    {
        return jobRunLogMapper.selectJobRunLogById(id);
    }

    /**
     * 查询运行任务日志列表
     * 
     * @param jobRunLog 运行任务日志
     * @return 运行任务日志
     */
    @Override
    public List<JobRunLog> selectJobRunLogList(JobRunLog jobRunLog)
    {
        return jobRunLogMapper.selectJobRunLogList(jobRunLog);
    }

    /**
     * 新增运行任务日志
     * 
     * @param jobRunLog 运行任务日志
     * @return 结果
     */
    @Override
    public int insertJobRunLog(JobRunLog jobRunLog)
    {
        jobRunLog.setCreateTime(DateUtils.getNowDate());
        return jobRunLogMapper.insertJobRunLog(jobRunLog);
    }

    /**
     * 修改运行任务日志
     * 
     * @param jobRunLog 运行任务日志
     * @return 结果
     */
    @Override
    public int updateJobRunLog(JobRunLog jobRunLog)
    {
        jobRunLog.setUpdateTime(DateUtils.getNowDate());
        return jobRunLogMapper.updateJobRunLog(jobRunLog);
    }

    /**
     * 删除运行任务日志对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteJobRunLogByIds(String ids)
    {
        return jobRunLogMapper.deleteJobRunLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除运行任务日志信息
     * 
     * @param id 运行任务日志ID
     * @return 结果
     */
    @Override
    public int deleteJobRunLogById(Long id)
    {
        return jobRunLogMapper.deleteJobRunLogById(id);
    }

	@Override
	public void updateLogById(String string, Long jobRunLogId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long insertJobRunLog(JobRunLogDTO jobRunLogDTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateJobRunLogById(JobRunLogDTO jobRunLogDTO) {
		// TODO Auto-generated method stub
		
	}
}
