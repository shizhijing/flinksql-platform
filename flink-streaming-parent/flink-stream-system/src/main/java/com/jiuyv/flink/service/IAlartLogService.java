package com.jiuyv.flink.service;

import java.util.List;
import com.jiuyv.flink.domain.AlartLog;

/**
 * 告警发送情况日志Service接口
 * 
 * @author shu_k
 * @date 2021-04-01
 */
public interface IAlartLogService 
{
    /**
     * 查询告警发送情况日志
     * 
     * @param id 告警发送情况日志ID
     * @return 告警发送情况日志
     */
    public AlartLog selectAlartLogById(Long id);

    /**
     * 查询告警发送情况日志列表
     * 
     * @param alartLog 告警发送情况日志
     * @return 告警发送情况日志集合
     */
    public List<AlartLog> selectAlartLogList(AlartLog alartLog);

    /**
     * 新增告警发送情况日志
     * 
     * @param alartLog 告警发送情况日志
     * @return 结果
     */
    public int insertAlartLog(AlartLog alartLog);

    /**
     * 修改告警发送情况日志
     * 
     * @param alartLog 告警发送情况日志
     * @return 结果
     */
    public int updateAlartLog(AlartLog alartLog);

    /**
     * 批量删除告警发送情况日志
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteAlartLogByIds(String ids);

    /**
     * 删除告警发送情况日志信息
     * 
     * @param id 告警发送情况日志ID
     * @return 结果
     */
    public int deleteAlartLogById(Long id);
}
