package com.jiuyv.flink.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.jiuyv.common.annotation.Excel;
import com.jiuyv.common.core.domain.BaseEntity;

/**
 * 告警辅助配置对象 flink_job_alarm_config
 * 
 * @author shu_k
 * @date 2021-04-01
 */
public class JobAlarmConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** job_config主表id */
    @Excel(name = "job_config主表id")
    private Long jobId;

    /** 类型 1:钉钉告警 2:url回调 3:异常自动拉起任务 */
    @Excel(name = "类型 1:钉钉告警 2:url回调 3:异常自动拉起任务")
    private Integer type;

    /** 更新版本号   */
    @Excel(name = "更新版本号  ")
    private Long version;

    /** $column.columnComment */
    @Excel(name = "更新版本号  ")
    private Integer isDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setJobId(Long jobId) 
    {
        this.jobId = jobId;
    }

    public Long getJobId() 
    {
        return jobId;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setVersion(Long version) 
    {
        this.version = version;
    }

    public Long getVersion() 
    {
        return version;
    }
    public void setIsDeleted(Integer isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Integer getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("jobId", getJobId())
            .append("type", getType())
            .append("version", getVersion())
            .append("isDeleted", getIsDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
