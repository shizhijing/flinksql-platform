package com.jiuyv.flink.service.impl;

import java.util.List;
import com.jiuyv.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiuyv.flink.mapper.AlartLogMapper;
import com.jiuyv.flink.domain.AlartLog;
import com.jiuyv.flink.service.IAlartLogService;
import com.jiuyv.common.core.text.Convert;

/**
 * 告警发送情况日志Service业务层处理
 * 
 * @author shu_k
 * @date 2021-04-01
 */
@Service
public class AlartLogServiceImpl implements IAlartLogService 
{
    @Autowired
    private AlartLogMapper alartLogMapper;

    /**
     * 查询告警发送情况日志
     * 
     * @param id 告警发送情况日志ID
     * @return 告警发送情况日志
     */
    @Override
    public AlartLog selectAlartLogById(Long id)
    {
        return alartLogMapper.selectAlartLogById(id);
    }

    /**
     * 查询告警发送情况日志列表
     * 
     * @param alartLog 告警发送情况日志
     * @return 告警发送情况日志
     */
    @Override
    public List<AlartLog> selectAlartLogList(AlartLog alartLog)
    {
        return alartLogMapper.selectAlartLogList(alartLog);
    }

    /**
     * 新增告警发送情况日志
     * 
     * @param alartLog 告警发送情况日志
     * @return 结果
     */
    @Override
    public int insertAlartLog(AlartLog alartLog)
    {
        alartLog.setCreateTime(DateUtils.getNowDate());
        return alartLogMapper.insertAlartLog(alartLog);
    }

    /**
     * 修改告警发送情况日志
     * 
     * @param alartLog 告警发送情况日志
     * @return 结果
     */
    @Override
    public int updateAlartLog(AlartLog alartLog)
    {
        alartLog.setUpdateTime(DateUtils.getNowDate());
        return alartLogMapper.updateAlartLog(alartLog);
    }

    /**
     * 删除告警发送情况日志对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteAlartLogByIds(String ids)
    {
        return alartLogMapper.deleteAlartLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除告警发送情况日志信息
     * 
     * @param id 告警发送情况日志ID
     * @return 结果
     */
    @Override
    public int deleteAlartLogById(Long id)
    {
        return alartLogMapper.deleteAlartLogById(id);
    }
}
