package com.jiuyv.flink.service;

import java.util.List;
import com.jiuyv.flink.domain.JobConfig;
import com.jiuyv.flink.streaming.dto.JobConfigDTO;
import com.jiuyv.flink.streaming.enums.YN;

/**
 * flink任务配置Service接口
 * 
 * @author shu_k
 * @date 2021-04-01
 */
public interface IJobConfigService 
{
    /**
     * 查询flink任务配置
     * 
     * @param id flink任务配置ID
     * @return flink任务配置
     */
    public JobConfig selectJobConfigById(Long id);

    /**
     * 查询flink任务配置列表
     * 
     * @param jobConfig flink任务配置
     * @return flink任务配置集合
     */
    public List<JobConfig> selectJobConfigList(JobConfig jobConfig);

    /**
     * 新增flink任务配置
     * 
     * @param jobConfig flink任务配置
     * @return 结果
     */
    public int insertJobConfig(JobConfig jobConfig);

    /**
     * 修改flink任务配置
     * 
     * @param jobConfig flink任务配置
     * @return 结果
     */
    public int updateJobConfig(JobConfig jobConfig);

    /**
     * 批量删除flink任务配置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteJobConfigByIds(String ids);

    /**
     * 删除flink任务配置信息
     * 
     * @param id flink任务配置ID
     * @return 结果
     */
    public int deleteJobConfigById(Long id);

	public void updateStatusByStart(Long id, String userName, Long jobRunLogId, Integer version);

	public void openOrClose(Long id, YN y, String userName);

	public List<JobConfigDTO> findJobConfigByStatus(Integer code);
	/**
	 * 	入库并返回主键
	 * 
	 * @param jobConfig
	 * @return
	 */
	public Long addJobConfig(JobConfig jobConfig);
}
