package com.jiuyv.flink.service;

import java.util.Date;
import java.util.List;
import com.jiuyv.flink.domain.SavepointBackup;

/**
 * savepoint备份地址Service接口
 * 
 * @author shu_k
 * @date 2021-04-01
 */
public interface ISavepointBackupService 
{
    /**
     * 查询savepoint备份地址
     * 
     * @param id savepoint备份地址ID
     * @return savepoint备份地址
     */
    public SavepointBackup selectSavepointBackupById(Long id);

    /**
     * 查询savepoint备份地址列表
     * 
     * @param savepointBackup savepoint备份地址
     * @return savepoint备份地址集合
     */
    public List<SavepointBackup> selectSavepointBackupList(SavepointBackup savepointBackup);

    /**
     * 新增savepoint备份地址
     * 
     * @param savepointBackup savepoint备份地址
     * @return 结果
     */
    public int insertSavepointBackup(SavepointBackup savepointBackup);

    /**
     * 修改savepoint备份地址
     * 
     * @param savepointBackup savepoint备份地址
     * @return 结果
     */
    public int updateSavepointBackup(SavepointBackup savepointBackup);

    /**
     * 批量删除savepoint备份地址
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSavepointBackupByIds(String ids);

    /**
     * 删除savepoint备份地址信息
     * 
     * @param id savepoint备份地址ID
     * @return 结果
     */
    public int deleteSavepointBackupById(Long id);

	public String getSavepointPathById(Long id, Long savepointId);

	public void insertSavepoint(Long id, String savepointPath, Date date);
}
