package com.jiuyv.flink.streaming.dto;

import org.springframework.util.CollectionUtils;

import com.jiuyv.common.utils.bean.BeanUtils;
import com.jiuyv.flink.domain.JobRunLog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author zhuhuipei
 * @date 2020-08-17
 * @time 00:14
 */
public class JobRunLogDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long jobConfigId;

    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 提交模式: standalone 、yarn 、yarn-session
     */
    private String deployMode;

    /**
     * 运行后的任务id
     */
    private String jobId;

    /**
     * 远程日志url的地址
     */
    private String remoteLogUrl;

    /**
     * 启动时间
     */
    private Date startTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 任务状态
     */
    private String jobStatus;


    private String creator;

    private String editor;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;


    /**
     * 启动时本地日志
     */
    private String localLog;


    public static JobRunLog toEntity(JobRunLogDTO jobRunLogDTO) {
        if (jobRunLogDTO == null) {
            return null;
        }
        JobRunLog jobRunLog = new JobRunLog();
        BeanUtils.copyBeanProp(jobRunLog, jobRunLogDTO);
        return jobRunLog;
    }


    public static JobRunLogDTO toDTO(JobRunLog jobRunLog) {
        if (jobRunLog == null) {
            return null;
        }
        JobRunLogDTO jobRunLogDTO = new JobRunLogDTO();
        BeanUtils.copyBeanProp(jobRunLogDTO, jobRunLog);
        return jobRunLogDTO;
    }

    public static List<JobRunLogDTO> toListDTO(List<JobRunLog> jobRunLogList) {
        if (CollectionUtils.isEmpty(jobRunLogList)) {
            return Collections.emptyList();
        }
        List<JobRunLogDTO> list = new ArrayList<>();

        for (JobRunLog jobRunLog : jobRunLogList) {
            list.add(JobRunLogDTO.toDTO(jobRunLog));
        }
        return list;


    }


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getJobConfigId() {
		return jobConfigId;
	}

	public void setJobConfigId(Long jobConfigId) {
		this.jobConfigId = jobConfigId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getDeployMode() {
		return deployMode;
	}

	public void setDeployMode(String deployMode) {
		this.deployMode = deployMode;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getRemoteLogUrl() {
		return remoteLogUrl;
	}

	public void setRemoteLogUrl(String remoteLogUrl) {
		this.remoteLogUrl = remoteLogUrl;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getLocalLog() {
		return localLog;
	}

	public void setLocalLog(String localLog) {
		this.localLog = localLog;
	}


}
