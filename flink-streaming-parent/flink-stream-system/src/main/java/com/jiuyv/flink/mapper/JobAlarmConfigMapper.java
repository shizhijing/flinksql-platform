package com.jiuyv.flink.mapper;

import java.util.List;
import com.jiuyv.flink.domain.JobAlarmConfig;

/**
 * 告警辅助配置Mapper接口
 * 
 * @author shu_k
 * @date 2021-04-01
 */
public interface JobAlarmConfigMapper 
{
    /**
     * 查询告警辅助配置
     * 
     * @param id 告警辅助配置ID
     * @return 告警辅助配置
     */
    public JobAlarmConfig selectJobAlarmConfigById(Long id);

    /**
     * 查询告警辅助配置列表
     * 
     * @param jobAlarmConfig 告警辅助配置
     * @return 告警辅助配置集合
     */
    public List<JobAlarmConfig> selectJobAlarmConfigList(JobAlarmConfig jobAlarmConfig);

    /**
     * 新增告警辅助配置
     * 
     * @param jobAlarmConfig 告警辅助配置
     * @return 结果
     */
    public int insertJobAlarmConfig(JobAlarmConfig jobAlarmConfig);

    /**
     * 修改告警辅助配置
     * 
     * @param jobAlarmConfig 告警辅助配置
     * @return 结果
     */
    public int updateJobAlarmConfig(JobAlarmConfig jobAlarmConfig);

    /**
     * 删除告警辅助配置
     * 
     * @param id 告警辅助配置ID
     * @return 结果
     */
    public int deleteJobAlarmConfigById(Long id);

    /**
     * 批量删除告警辅助配置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteJobAlarmConfigByIds(String[] ids);
}
