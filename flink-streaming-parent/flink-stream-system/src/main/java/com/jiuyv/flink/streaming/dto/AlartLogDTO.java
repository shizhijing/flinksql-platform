package com.jiuyv.flink.streaming.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.flink.util.CollectionUtil;

import com.jiuyv.common.utils.bean.BeanUtils;
import com.jiuyv.flink.domain.AlartLog;
import com.jiuyv.flink.streaming.enums.AlarmLogStatusEnum;
import com.jiuyv.flink.streaming.enums.AlarmLogTypeEnum;

/**
 * @author
 */
public class AlartLogDTO {

    private Long id;

    private Long jobConfigId;

    private String jobName;

    /**
     * 消息内容
     */
    private String message;

    /**
     * 1:钉钉
     */
    private AlarmLogTypeEnum alarMLogTypeEnum;

    /**
     * 1:成功 0:失败
     */
    private AlarmLogStatusEnum alarmLogStatusEnum;

    /**
     * 失败原因
     */
    private String failLog;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date editTime;

    private String createBy;

    private String updateBy;
    


    public static AlartLog toEntity(AlartLogDTO alartLogDTO) {
        if (alartLogDTO == null) {
            return null;
        }
        AlartLog alartLog = new AlartLog();
        BeanUtils.copyBeanProp(alartLog, alartLogDTO);
        return alartLog;
    }

    public static AlartLogDTO toDTO(AlartLog alartLog) {
        if (alartLog == null) {
            return null;
        }
        AlartLogDTO alartLogDTO = new AlartLogDTO();
        BeanUtils.copyBeanProp(alartLogDTO, alartLog);
        return alartLogDTO;
    }

    public static List<AlartLogDTO> toListDTO(List<AlartLog> alartLogList) {
        if (CollectionUtil.isNullOrEmpty(alartLogList)) {
            return Collections.emptyList();
        }
        List<AlartLogDTO> list = new ArrayList<>();
        for (AlartLog alartLog : alartLogList) {
            AlartLogDTO alartLogDTO = AlartLogDTO.toDTO(alartLog);
            if (alartLog != null) {
                list.add(alartLogDTO);
            }
        }
        return list;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getJobConfigId() {
		return jobConfigId;
	}

	public void setJobConfigId(Long jobConfigId) {
		this.jobConfigId = jobConfigId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public AlarmLogTypeEnum getAlarMLogTypeEnum() {
		return alarMLogTypeEnum;
	}

	public void setAlarMLogTypeEnum(AlarmLogTypeEnum alarMLogTypeEnum) {
		this.alarMLogTypeEnum = alarMLogTypeEnum;
	}

	public AlarmLogStatusEnum getAlarmLogStatusEnum() {
		return alarmLogStatusEnum;
	}

	public void setAlarmLogStatusEnum(AlarmLogStatusEnum alarmLogStatusEnum) {
		this.alarmLogStatusEnum = alarmLogStatusEnum;
	}

	public String getFailLog() {
		return failLog;
	}

	public void setFailLog(String failLog) {
		this.failLog = failLog;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getEditTime() {
		return editTime;
	}

	public void setEditTime(Date editTime) {
		this.editTime = editTime;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

    
}
