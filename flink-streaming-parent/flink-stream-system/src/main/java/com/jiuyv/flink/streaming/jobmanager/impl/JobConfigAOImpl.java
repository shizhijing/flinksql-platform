package com.jiuyv.flink.streaming.jobmanager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.jiuyv.flink.service.IJobAlarmConfigService;
import com.jiuyv.flink.service.IJobConfigService;
import com.jiuyv.flink.streaming.dto.JobConfigDTO;
import com.jiuyv.flink.streaming.jobmanager.JobConfigAO;

/**
 * @author zhuhuipei
 * @Description:
 * @date 2021/2/28
 * @time 11:25
 */
@Component
public class JobConfigAOImpl implements JobConfigAO {

    @Autowired
    private IJobConfigService jobConfigService;

    @Autowired
    private IJobAlarmConfigService jobAlarmConfigService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addJobConfig(JobConfigDTO jobConfigDTO) {
        Long jobConfigId = jobConfigService.addJobConfig(JobConfigDTO.toEntity(jobConfigDTO));
        jobAlarmConfigService.upSertBatchJobAlarmConfig(jobConfigDTO.getAlarmTypeEnumList(), jobConfigId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateJobConfigById(JobConfigDTO jobConfigDTO) {
        jobConfigService.updateJobConfig(JobConfigDTO.toEntity(jobConfigDTO));
        jobAlarmConfigService.upSertBatchJobAlarmConfig(jobConfigDTO.getAlarmTypeEnumList(), jobConfigDTO.getId());
    }
}
