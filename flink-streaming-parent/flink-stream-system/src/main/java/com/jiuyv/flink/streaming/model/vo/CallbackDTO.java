package com.jiuyv.flink.streaming.model.vo;

import com.jiuyv.flink.streaming.dto.JobConfigDTO;

/**
 * @author zhuhuipei
 * @Description:
 * @date 2021/2/21
 * @time 22:20
 */
public class CallbackDTO {

    private String appId;

    private String jobName;

    private String deployMode;

    private Long jobConfigId;


    public static CallbackDTO to(JobConfigDTO jobConfigDTO) {
        CallbackDTO callbackDTO = new CallbackDTO();
        callbackDTO.setJobConfigId(jobConfigDTO.getId());
        callbackDTO.setJobName(jobConfigDTO.getJobName());
        callbackDTO.setDeployMode(jobConfigDTO.getDeployModeEnum().name());
        callbackDTO.setAppId(jobConfigDTO.getJobId());
        return callbackDTO;
    }


	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getDeployMode() {
		return deployMode;
	}

	public void setDeployMode(String deployMode) {
		this.deployMode = deployMode;
	}

	public Long getJobConfigId() {
		return jobConfigId;
	}

	public void setJobConfigId(Long jobConfigId) {
		this.jobConfigId = jobConfigId;
	}
    
    
}
