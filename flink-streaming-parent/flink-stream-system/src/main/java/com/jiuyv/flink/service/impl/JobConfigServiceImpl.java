package com.jiuyv.flink.service.impl;

import java.util.List;
import com.jiuyv.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiuyv.flink.mapper.JobConfigMapper;
import com.jiuyv.flink.domain.JobConfig;
import com.jiuyv.flink.service.IJobConfigService;
import com.jiuyv.flink.streaming.dto.JobConfigDTO;
import com.jiuyv.flink.streaming.enums.YN;
import com.jiuyv.common.core.text.Convert;

/**
 * flink任务配置Service业务层处理
 * 
 * @author shu_k
 * @date 2021-04-01
 */
@Service
public class JobConfigServiceImpl implements IJobConfigService 
{
    @Autowired
    private JobConfigMapper jobConfigMapper;

    /**
     * 查询flink任务配置
     * 
     * @param id flink任务配置ID
     * @return flink任务配置
     */
    @Override
    public JobConfig selectJobConfigById(Long id)
    {
        return jobConfigMapper.selectJobConfigById(id);
    }

    /**
     * 查询flink任务配置列表
     * 
     * @param jobConfig flink任务配置
     * @return flink任务配置
     */
    @Override
    public List<JobConfig> selectJobConfigList(JobConfig jobConfig)
    {
        return jobConfigMapper.selectJobConfigList(jobConfig);
    }

    /**
     * 新增flink任务配置
     * 
     * @param jobConfig flink任务配置
     * @return 结果
     */
    @Override
    public int insertJobConfig(JobConfig jobConfig)
    {
        jobConfig.setCreateTime(DateUtils.getNowDate());
        return jobConfigMapper.insertJobConfig(jobConfig);
    }

    /**
     * 修改flink任务配置
     * 
     * @param jobConfig flink任务配置
     * @return 结果
     */
    @Override
    public int updateJobConfig(JobConfig jobConfig)
    {
        jobConfig.setUpdateTime(DateUtils.getNowDate());
        return jobConfigMapper.updateJobConfig(jobConfig);
    }

    /**
     * 删除flink任务配置对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteJobConfigByIds(String ids)
    {
        return jobConfigMapper.deleteJobConfigByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除flink任务配置信息
     * 
     * @param id flink任务配置ID
     * @return 结果
     */
    @Override
    public int deleteJobConfigById(Long id)
    {
        return jobConfigMapper.deleteJobConfigById(id);
    }

	@Override
	public Long addJobConfig(JobConfig jobConfig) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void updateStatusByStart(Long id, String userName, Long jobRunLogId, Integer version) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void openOrClose(Long id, YN y, String userName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<JobConfigDTO> findJobConfigByStatus(Integer code) {
		// TODO Auto-generated method stub
		return null;
	}
}
