package com.jiuyv.flink.streaming.dto;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.flink.util.CollectionUtil;

import com.jiuyv.flink.domain.SavepointBackup;

/**
 * @author
 */
public class SavepointBackupDTO {

    private Long id;

    private Long jobConfigId;

    /**
     * backup地址
     */
    private String savepointPath;


    private Date backupTime;


    public static SavepointBackupDTO toDTO(SavepointBackup savepointBackup) {
        if (savepointBackup == null) {
            return null;
        }
        SavepointBackupDTO savepointBackupDTO = new SavepointBackupDTO();
        savepointBackupDTO.setId(savepointBackup.getId());
        savepointBackupDTO.setJobConfigId(savepointBackup.getJobConfigId());
        savepointBackupDTO.setSavepointPath(savepointBackup.getSavepointPath());
        savepointBackupDTO.setBackupTime(savepointBackup.getBackupTime());
        return savepointBackupDTO;
    }

    public static List<SavepointBackupDTO> toDTOList(List<SavepointBackup> savepointBackupList) {
        if (CollectionUtil.isNullOrEmpty(savepointBackupList)) {
            return Collections.emptyList();
        }
        List<SavepointBackupDTO> list = new ArrayList<SavepointBackupDTO>();
        for (SavepointBackup savepointBackup : savepointBackupList) {
            list.add(toDTO(savepointBackup));
        }
        return list;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getJobConfigId() {
		return jobConfigId;
	}

	public void setJobConfigId(Long jobConfigId) {
		this.jobConfigId = jobConfigId;
	}

	public String getSavepointPath() {
		return savepointPath;
	}

	public void setSavepointPath(String savepointPath) {
		this.savepointPath = savepointPath;
	}

	public Date getBackupTime() {
		return backupTime;
	}

	public void setBackupTime(Date backupTime) {
		this.backupTime = backupTime;
	}


}
