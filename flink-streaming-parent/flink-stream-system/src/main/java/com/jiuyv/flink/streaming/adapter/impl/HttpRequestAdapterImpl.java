package com.jiuyv.flink.streaming.adapter.impl;

import com.alibaba.fastjson.JSON;
import com.jiuyv.flink.service.ISystemConfigService;
import com.jiuyv.flink.streaming.adapter.HttpRequestAdapter;
import com.jiuyv.flink.streaming.common.constant.SystemConstants;
import com.jiuyv.flink.streaming.dto.JobConfigDTO;
import com.jiuyv.flink.streaming.enums.SysErrorEnum;
import com.jiuyv.flink.streaming.enums.YarnStateEnum;
import com.jiuyv.flink.streaming.exception.BizException;
import com.jiuyv.flink.streaming.model.to.AppTO;
import com.jiuyv.flink.streaming.model.to.YarnAppInfo;
import com.jiuyv.flink.utils.HttpUtil;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author zhuhuipei
 * @Description:
 * @date 2020-08-06
 * @time 20:15
 */
@Service
public class HttpRequestAdapterImpl implements HttpRequestAdapter {

	private static final Logger log = LoggerFactory.getLogger(HttpRequestAdapterImpl.class);
    private static final String BODY_HTTP_KILL = "{\"state\":\"KILLED\"}";

    @Autowired
    private ISystemConfigService systemConfigService;

    //TODO 设个方法设计不好 需要改造
    @Override
    public String getAppIdByYarn(String jobName, String queueName) {
        if (StringUtils.isEmpty(jobName)) {
            throw new BizException(SysErrorEnum.PARAM_IS_NULL);
        }
        String url = systemConfigService.getYarnRmHttpAddress() + SystemConstants.buildHttpQuery(queueName);
        RestTemplate restTemplate = HttpUtil.buildRestTemplate(HttpUtil.TIME_OUT_1_M);
        log.info("请求参数  url={}", url);
        String res = restTemplate.getForObject(url, String.class);
        log.info("请求结果 str={} url={}", res, url);

        YarnAppInfo yarnAppInfo = JSON.parseObject(res, YarnAppInfo.class);

        this.check(yarnAppInfo, queueName, jobName, url);

        for (AppTO appTO : yarnAppInfo.getApps().getApp()) {
            if (JobConfigDTO.buildRunName(jobName).equals(appTO.getName()) ) {
                if ( SystemConstants.STATUS_RUNNING.equals(appTO.getState())){
                    log.info("任务信息 appTO={}", appTO);
                    return appTO.getId();
                }else{
                    log.error("任务运行状态失败 状态是 {}",appTO.getState());
                }
            }
        }
        throw new BizException("yarn队列" + queueName + "中没有找到运行的任务 name=" +
                JobConfigDTO.buildRunName(jobName), SysErrorEnum.YARN_CODE.getCode());
    }


    @Override
    public void stopJobByJobId(String appId) {
        log.info("执行stopJobByJobId appId={}", appId);
        if (StringUtils.isEmpty(appId)) {
            throw new BizException(SysErrorEnum.PARAM_IS_NULL_YARN_APPID);
        }
        String url = systemConfigService.getYarnRmHttpAddress() + SystemConstants.HTTP_YARN_APPS + appId + "/state";
        log.info("请求关闭 URL ={}", url);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> httpEntity = new HttpEntity<String>(BODY_HTTP_KILL, headers);
        RestTemplate restTemplate = HttpUtil.buildRestTemplate(HttpUtil.TIME_OUT_1_M);
        restTemplate.put(url, httpEntity);
    }

    @Override
    public YarnStateEnum getJobStateByJobId(String appId) {
        if (StringUtils.isEmpty(appId)) {
            throw new BizException(SysErrorEnum.PARAM_IS_NULL_YARN_APPID);
        }

        String url = systemConfigService.getYarnRmHttpAddress() + SystemConstants.HTTP_YARN_APPS + appId + "/state";
        RestTemplate restTemplate = HttpUtil.buildRestTemplate(HttpUtil.TIME_OUT_1_M);
        String res = restTemplate.getForObject(url, String.class);
        if (StringUtils.isEmpty(res)) {
            log.error("请求失败:返回结果为空 url={}", url);
            throw new BizException(SysErrorEnum.HTTP_REQUEST_IS_NULL);
        }
        return YarnStateEnum.getYarnStateEnum(String.valueOf(JSON.parseObject(res).get("state")));
    }


    private void check(YarnAppInfo yarnAppInfo,String queueName,String jobName,String url){
        if (yarnAppInfo == null) {
            log.error("在队列" + queueName + "没有找到任何yarn上的任务 url={}", url);
            throw new BizException("yarn队列" + queueName + "中没有找到运行的任务 name=" +
                    JobConfigDTO.buildRunName(jobName), SysErrorEnum.YARN_CODE.getCode());
        }
        if (yarnAppInfo.getApps() == null) {
            log.error("yarnAppInfo.getApps() is null", yarnAppInfo);
            throw new BizException("yarn队列" + queueName + "中没有找到运行的任务 name=" +
                    JobConfigDTO.buildRunName(jobName), SysErrorEnum.YARN_CODE.getCode());
        }
        if (yarnAppInfo.getApps().getApp()==null||yarnAppInfo.getApps().getApp().size()<=0){
            log.error("yarnAppInfo.getApps().getApp() is null", yarnAppInfo);
            throw new BizException("yarn队列" + queueName + "中没有找到运行的任务 name=" +
                    JobConfigDTO.buildRunName(jobName), SysErrorEnum.YARN_CODE.getCode());
        }
    }

}
