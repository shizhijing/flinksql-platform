package com.jiuyv.flink.mapper;

import java.util.List;
import com.jiuyv.flink.domain.UdfConfig;

/**
 * udf函数管理Mapper接口
 * 
 * @author shu_k
 * @date 2021-04-01
 */
public interface UdfConfigMapper 
{
    /**
     * 查询udf函数管理
     * 
     * @param id udf函数管理ID
     * @return udf函数管理
     */
    public UdfConfig selectUdfConfigById(Long id);

    /**
     * 查询udf函数管理列表
     * 
     * @param udfConfig udf函数管理
     * @return udf函数管理集合
     */
    public List<UdfConfig> selectUdfConfigList(UdfConfig udfConfig);

    /**
     * 新增udf函数管理
     * 
     * @param udfConfig udf函数管理
     * @return 结果
     */
    public int insertUdfConfig(UdfConfig udfConfig);

    /**
     * 修改udf函数管理
     * 
     * @param udfConfig udf函数管理
     * @return 结果
     */
    public int updateUdfConfig(UdfConfig udfConfig);

    /**
     * 删除udf函数管理
     * 
     * @param id udf函数管理ID
     * @return 结果
     */
    public int deleteUdfConfigById(Long id);

    /**
     * 批量删除udf函数管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUdfConfigByIds(String[] ids);
}
