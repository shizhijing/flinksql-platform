package com.jiuyv.flink.streaming.enums;


/**
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author  shu_k
 * @date 2021年4月2日 下午3:30:50
 */
public enum AlarmTypeEnum {
	
    DINGDING(1, "钉钉告警"),
    CALLBACK_URL(2, "回调http告警"),
    AUTO_START_JOB(3, "任务退出自动拉起"),
    ;

    private int code;

    private String desc;
    

    public int getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	AlarmTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static AlarmTypeEnum getAlarmTypeEnum(Integer code) {
        if (code == null) {
            return null;
        }

        for (AlarmTypeEnum alarMLogTypeEnum : AlarmTypeEnum.values()) {
            if (alarMLogTypeEnum.getCode() == code.intValue()) {
                return alarMLogTypeEnum;
            }

        }
        return null;
    }
}
