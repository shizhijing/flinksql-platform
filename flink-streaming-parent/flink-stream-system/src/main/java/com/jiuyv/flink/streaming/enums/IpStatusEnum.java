package com.jiuyv.flink.streaming.enums;


/**
 * @author zhuhuipei
 * @Description:
 * @date 2020-07-20
 * @time 02:15
 */
public enum IpStatusEnum {

    START(1), STOP(-1);

    private int code;

    public int getCode() {
		return code;
	}

	IpStatusEnum(int code) {
        this.code = code;
    }


}
