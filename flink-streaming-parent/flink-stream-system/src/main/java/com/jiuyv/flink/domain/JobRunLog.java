package com.jiuyv.flink.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.jiuyv.common.annotation.Excel;
import com.jiuyv.common.core.domain.BaseEntity;

/**
 * 运行任务日志对象 flink_job_run_log
 * 
 * @author shu_k
 * @date 2021-04-01
 */
public class JobRunLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long jobConfigId;

    /** 任务名称 */
    @Excel(name = "任务名称")
    private String jobName;

    /** 提交模式: standalone 、yarn 、yarn-session  */
    @Excel(name = "提交模式: standalone 、yarn 、yarn-session ")
    private String deployMode;

    /** 运行后的任务id */
    @Excel(name = "运行后的任务id")
    private String jobId;

    /** 启动时本地日志 */
    @Excel(name = "启动时本地日志")
    private String localLog;

    /** 远程日志url的地址 */
    @Excel(name = "远程日志url的地址")
    private String remoteLogUrl;

    /** 启动时间 */
    @Excel(name = "启动时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 启动时间 */
    @Excel(name = "启动时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 任务状态 */
    @Excel(name = "任务状态")
    private String jobStatus;

    /** $column.columnComment */
    @Excel(name = "任务状态")
    private Integer isDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setJobConfigId(Long jobConfigId) 
    {
        this.jobConfigId = jobConfigId;
    }

    public Long getJobConfigId() 
    {
        return jobConfigId;
    }
    public void setJobName(String jobName) 
    {
        this.jobName = jobName;
    }

    public String getJobName() 
    {
        return jobName;
    }
    public void setDeployMode(String deployMode) 
    {
        this.deployMode = deployMode;
    }

    public String getDeployMode() 
    {
        return deployMode;
    }
    public void setJobId(String jobId) 
    {
        this.jobId = jobId;
    }

    public String getJobId() 
    {
        return jobId;
    }
    public void setLocalLog(String localLog) 
    {
        this.localLog = localLog;
    }

    public String getLocalLog() 
    {
        return localLog;
    }
    public void setRemoteLogUrl(String remoteLogUrl) 
    {
        this.remoteLogUrl = remoteLogUrl;
    }

    public String getRemoteLogUrl() 
    {
        return remoteLogUrl;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setJobStatus(String jobStatus) 
    {
        this.jobStatus = jobStatus;
    }

    public String getJobStatus() 
    {
        return jobStatus;
    }
    public void setIsDeleted(Integer isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Integer getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("jobConfigId", getJobConfigId())
            .append("jobName", getJobName())
            .append("deployMode", getDeployMode())
            .append("jobId", getJobId())
            .append("localLog", getLocalLog())
            .append("remoteLogUrl", getRemoteLogUrl())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("jobStatus", getJobStatus())
            .append("isDeleted", getIsDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
