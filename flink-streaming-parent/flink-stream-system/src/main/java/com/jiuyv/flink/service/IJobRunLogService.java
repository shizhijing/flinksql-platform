package com.jiuyv.flink.service;

import java.util.List;
import com.jiuyv.flink.domain.JobRunLog;
import com.jiuyv.flink.streaming.dto.JobRunLogDTO;

/**
 * 运行任务日志Service接口
 * 
 * @author shu_k
 * @date 2021-04-01
 */
public interface IJobRunLogService 
{
    /**
     * 查询运行任务日志
     * 
     * @param id 运行任务日志ID
     * @return 运行任务日志
     */
    public JobRunLog selectJobRunLogById(Long id);

    /**
     * 查询运行任务日志列表
     * 
     * @param jobRunLog 运行任务日志
     * @return 运行任务日志集合
     */
    public List<JobRunLog> selectJobRunLogList(JobRunLog jobRunLog);

    /**
     * 新增运行任务日志
     * 
     * @param jobRunLog 运行任务日志
     * @return 结果
     */
    public int insertJobRunLog(JobRunLog jobRunLog);

    /**
     * 修改运行任务日志
     * 
     * @param jobRunLog 运行任务日志
     * @return 结果
     */
    public int updateJobRunLog(JobRunLog jobRunLog);

    /**
     * 批量删除运行任务日志
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteJobRunLogByIds(String ids);

    /**
     * 删除运行任务日志信息
     * 
     * @param id 运行任务日志ID
     * @return 结果
     */
    public int deleteJobRunLogById(Long id);

	public void updateLogById(String string, Long jobRunLogId);

	public Long insertJobRunLog(JobRunLogDTO jobRunLogDTO);

	public void updateJobRunLogById(JobRunLogDTO jobRunLogDTO);
}
