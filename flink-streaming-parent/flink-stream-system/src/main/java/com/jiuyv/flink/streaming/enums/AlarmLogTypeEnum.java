package com.jiuyv.flink.streaming.enums;


/**
 * @Description: 回调类型
 * @author  shu_k
 * @date 2021年4月2日 下午3:30:34
 */
public enum AlarmLogTypeEnum {

    DINGDING(1, "钉钉"),
    CALLBACK_URL(2, "自定义回调http");

    private int code;

    private String desc;

    public int getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	AlarmLogTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static AlarmLogTypeEnum getAlarmLogTypeEnum(Integer code) {
        if (code == null) {
            return null;
        }

        for (AlarmLogTypeEnum alarMLogTypeEnum : AlarmLogTypeEnum.values()) {
            if (alarMLogTypeEnum.getCode() == code.intValue()) {
                return alarMLogTypeEnum;
            }

        }
        return null;
    }


}
