package com.jiuyv.flink.streaming.adapter;

import com.jiuyv.flink.streaming.enums.YarnStateEnum;

/**
 * @Description: Flink 的jobManager提供的rest api 接口
 * @author  shu_k
 * @date 2021年4月2日 下午3:15:17
 */
public interface HttpRequestAdapter {

    /**
     * 	通过任务名称获取yarn 的appId
     *
     * @author shu_k
     * @date 2020-08-06
     * @time 20:18
     */
    String getAppIdByYarn(String jobName, String queueName);

    /**
     * 	通过http杀掉一个任务
     *
     * @author shu_k
     * @date 2020-08-06
     * @time 20:50
     */
    void stopJobByJobId(String appId);


    /**
     * 	查询yarn 上某任务状态
     *
     * @author shu_k
     * @date 2020-08-07
     * @time 21:36
     */
    YarnStateEnum getJobStateByJobId(String appId);


}
