package com.jiuyv.flink.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.jiuyv.common.annotation.Excel;
import com.jiuyv.common.core.domain.BaseEntity;

/**
 * 系统配置对象 flink_system_config
 * 
 * @author shu_k
 * @date 2021-04-01
 */
public class SystemConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** key值 */
    @Excel(name = "key值")
    private String sysKey;

    /** value */
    @Excel(name = "value")
    private String sysVal;

    /** 类型 如:sys  alarm */
    @Excel(name = "类型 如:sys  alarm")
    private String type;

    /** $column.columnComment */
    @Excel(name = "类型 如:sys  alarm")
    private Integer isDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSysKey(String sysKey) 
    {
        this.sysKey = sysKey;
    }

    public String getSysKey() 
    {
        return sysKey;
    }
    public void setSysVal(String sysVal) 
    {
        this.sysVal = sysVal;
    }

    public String getSysVal() 
    {
        return sysVal;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setIsDeleted(Integer isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Integer getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sysKey", getSysKey())
            .append("sysVal", getSysVal())
            .append("type", getType())
            .append("isDeleted", getIsDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
