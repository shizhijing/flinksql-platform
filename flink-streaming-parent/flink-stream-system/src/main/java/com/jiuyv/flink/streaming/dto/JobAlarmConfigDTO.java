package com.jiuyv.flink.streaming.dto;

import org.apache.commons.collections.CollectionUtils;

import com.jiuyv.common.utils.bean.BeanUtils;
import com.jiuyv.flink.domain.JobAlarmConfig;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author zhuhuipei
 * @date 2021/2/27
 * @time 17:09
 */
public class JobAlarmConfigDTO {

    private Long id;

    /**
     * job_config主表id
     */
    private Long jobId;

    /**
     * 类型 1:钉钉告警 2:url回调 3:异常自动拉起任务
     */
    private Integer type;


    public static JobAlarmConfigDTO toDTO(JobAlarmConfig jobAlarmConfig) {
        if (jobAlarmConfig == null) {
            return null;
        }
        JobAlarmConfigDTO jobAlarmConfigDTO = new JobAlarmConfigDTO();
        BeanUtils.copyBeanProp(jobAlarmConfigDTO, jobAlarmConfig);
        return jobAlarmConfigDTO;
    }

    public static JobAlarmConfig toEntity(JobAlarmConfigDTO jobAlarmConfigDTO) {
        if (jobAlarmConfigDTO == null) {
            return null;
        }
        JobAlarmConfig jobAlarmConfig = new JobAlarmConfig();
        BeanUtils.copyBeanProp(jobAlarmConfig, jobAlarmConfigDTO);
        return jobAlarmConfig;
    }

    public static List<JobAlarmConfig> toEntityList(List<JobAlarmConfigDTO> jobAlarmConfigDTOList) {
        if (CollectionUtils.isEmpty(jobAlarmConfigDTOList)) {
            return Collections.emptyList();
        }
        List<JobAlarmConfig> list = new ArrayList<>();

        for (JobAlarmConfigDTO jobAlarmConfigDTO : jobAlarmConfigDTOList) {
            list.add(toEntity(jobAlarmConfigDTO));
        }
        return list;
    }


    public static List<JobAlarmConfigDTO> toListDTO(List<JobAlarmConfig> jobAlarmConfigList) {
        if (CollectionUtils.isEmpty(jobAlarmConfigList)) {
            return Collections.emptyList();
        }
        List<JobAlarmConfigDTO> list = new ArrayList<>();

        for (JobAlarmConfig jobAlarmConfig : jobAlarmConfigList) {
            list.add(toDTO(jobAlarmConfig));
        }
        return list;

    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}


}
