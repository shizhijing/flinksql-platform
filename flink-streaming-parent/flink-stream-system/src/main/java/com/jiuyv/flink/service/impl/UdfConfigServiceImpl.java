package com.jiuyv.flink.service.impl;

import java.util.List;
import com.jiuyv.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiuyv.flink.mapper.UdfConfigMapper;
import com.jiuyv.flink.domain.UdfConfig;
import com.jiuyv.flink.service.IUdfConfigService;
import com.jiuyv.common.core.text.Convert;

/**
 * udf函数管理Service业务层处理
 * 
 * @author shu_k
 * @date 2021-04-01
 */
@Service
public class UdfConfigServiceImpl implements IUdfConfigService 
{
    @Autowired
    private UdfConfigMapper udfConfigMapper;

    /**
     * 查询udf函数管理
     * 
     * @param id udf函数管理ID
     * @return udf函数管理
     */
    @Override
    public UdfConfig selectUdfConfigById(Long id)
    {
        return udfConfigMapper.selectUdfConfigById(id);
    }

    /**
     * 查询udf函数管理列表
     * 
     * @param udfConfig udf函数管理
     * @return udf函数管理
     */
    @Override
    public List<UdfConfig> selectUdfConfigList(UdfConfig udfConfig)
    {
        return udfConfigMapper.selectUdfConfigList(udfConfig);
    }

    /**
     * 新增udf函数管理
     * 
     * @param udfConfig udf函数管理
     * @return 结果
     */
    @Override
    public int insertUdfConfig(UdfConfig udfConfig)
    {
        udfConfig.setCreateTime(DateUtils.getNowDate());
        return udfConfigMapper.insertUdfConfig(udfConfig);
    }

    /**
     * 修改udf函数管理
     * 
     * @param udfConfig udf函数管理
     * @return 结果
     */
    @Override
    public int updateUdfConfig(UdfConfig udfConfig)
    {
        udfConfig.setUpdateTime(DateUtils.getNowDate());
        return udfConfigMapper.updateUdfConfig(udfConfig);
    }

    /**
     * 删除udf函数管理对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUdfConfigByIds(String ids)
    {
        return udfConfigMapper.deleteUdfConfigByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除udf函数管理信息
     * 
     * @param id udf函数管理ID
     * @return 结果
     */
    @Override
    public int deleteUdfConfigById(Long id)
    {
        return udfConfigMapper.deleteUdfConfigById(id);
    }
}
