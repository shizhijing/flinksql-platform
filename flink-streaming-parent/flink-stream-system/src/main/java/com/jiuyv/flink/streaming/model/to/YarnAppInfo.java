package com.jiuyv.flink.streaming.model.to;


/**
 * @author zhuhuipei
 * @Description:
 * @date 2020-08-06
 * @time 02:04
 */
public class YarnAppInfo {

    private AppListTO apps;

	public AppListTO getApps() {
		return apps;
	}
	public void setApps(AppListTO apps) {
		this.apps = apps;
	}
    
    
}
