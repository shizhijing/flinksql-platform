package com.jiuyv.flink.streaming.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author zhuhuipei
 * @Description:
 * @date 2020-08-15
 * @time 20:41
 */
public enum DeployModeEnum {
    YARN_PER, STANDALONE, LOCAL;

    public static DeployModeEnum getModel(String model) {
        if (StringUtils.isEmpty(model)) {
            return null;
        }
        for (DeployModeEnum deployModeEnum : DeployModeEnum.values()) {
            if (deployModeEnum.name().equalsIgnoreCase(model.trim())) {
                return deployModeEnum;
            }

        }
		return null;
        
    }
}
