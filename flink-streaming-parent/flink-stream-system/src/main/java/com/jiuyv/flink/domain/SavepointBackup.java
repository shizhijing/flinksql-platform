package com.jiuyv.flink.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.jiuyv.common.annotation.Excel;
import com.jiuyv.common.core.domain.BaseEntity;

/**
 * savepoint备份地址对象 flink_savepoint_backup
 * 
 * @author shu_k
 * @date 2021-04-01
 */
public class SavepointBackup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long jobConfigId;

    /** 地址 */
    @Excel(name = "地址")
    private String savepointPath;

    /** 备份时间 */
    @Excel(name = "备份时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date backupTime;

    /** $column.columnComment */
    @Excel(name = "备份时间")
    private Integer isDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setJobConfigId(Long jobConfigId) 
    {
        this.jobConfigId = jobConfigId;
    }

    public Long getJobConfigId() 
    {
        return jobConfigId;
    }
    public void setSavepointPath(String savepointPath) 
    {
        this.savepointPath = savepointPath;
    }

    public String getSavepointPath() 
    {
        return savepointPath;
    }
    public void setBackupTime(Date backupTime) 
    {
        this.backupTime = backupTime;
    }

    public Date getBackupTime() 
    {
        return backupTime;
    }
    public void setIsDeleted(Integer isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Integer getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("jobConfigId", getJobConfigId())
            .append("savepointPath", getSavepointPath())
            .append("backupTime", getBackupTime())
            .append("isDeleted", getIsDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
