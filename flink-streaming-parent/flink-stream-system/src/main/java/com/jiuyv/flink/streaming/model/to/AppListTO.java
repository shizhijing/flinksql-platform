package com.jiuyv.flink.streaming.model.to;


import java.util.List;

/**
 * @author zhuhuipei
 * @Description:
 * @date 2020-08-06
 * @time 02:04
 */
public class AppListTO {

    private List<AppTO> app;

	public List<AppTO> getApp() {
		return app;
	}

	public void setApp(List<AppTO> app) {
		this.app = app;
	}
    
    
}
