package com.jiuyv.flink.streaming.enums;


/**
 * @author zhuhuipei
 * @Description:
 * @date 2020-07-10
 * @time 00:59
 */
public enum JobConfigStatus {

    FAIL(-1, "失败"),

    RUN(1, "运行中"),

    STOP(0, "停止"),

    STARTING(2, "启动中"),

    UNKNOWN(-2, "未知"),
    ;

    private Integer code;

    private String desc;

    public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	JobConfigStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static JobConfigStatus getJobConfigStatus(Integer code) {
        for (JobConfigStatus jobConfigStatus : JobConfigStatus.values()) {
            if (jobConfigStatus.getCode().equals(code)) {
                return jobConfigStatus;
            }
        }
        return JobConfigStatus.UNKNOWN;
    }

}
