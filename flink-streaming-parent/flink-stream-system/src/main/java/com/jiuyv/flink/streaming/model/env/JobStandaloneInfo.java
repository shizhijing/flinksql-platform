package com.jiuyv.flink.streaming.model.env;


/**
 * @Description: 
 * @author  shu_k
 * @date 2021年4月2日 下午3:12:00
 */
public class JobStandaloneInfo {

    private String jid;

    private String state;

    private String errors;
    
    public JobStandaloneInfo() {
    	
	}

	public JobStandaloneInfo(String jid, String state, String errors) {
		this.jid = jid;
		this.state = state;
		this.errors = errors;
	}

	public String getJid() {
		return jid;
	}

	public void setJid(String jid) {
		this.jid = jid;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getErrors() {
		return errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

}
