package com.jiuyv.flink.service.impl;

import java.util.List;
import com.jiuyv.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiuyv.flink.mapper.IpStatusMapper;
import com.jiuyv.flink.domain.IpStatus;
import com.jiuyv.flink.service.IIpStatusService;
import com.jiuyv.common.core.text.Convert;

/**
 * web服务运行ipService业务层处理
 * 
 * @author shu_k
 * @date 2021-04-01
 */
@Service
public class IpStatusServiceImpl implements IIpStatusService 
{
    @Autowired
    private IpStatusMapper ipStatusMapper;

    /**
     * 查询web服务运行ip
     * 
     * @param id web服务运行ipID
     * @return web服务运行ip
     */
    @Override
    public IpStatus selectIpStatusById(Long id)
    {
        return ipStatusMapper.selectIpStatusById(id);
    }

    /**
     * 查询web服务运行ip列表
     * 
     * @param ipStatus web服务运行ip
     * @return web服务运行ip
     */
    @Override
    public List<IpStatus> selectIpStatusList(IpStatus ipStatus)
    {
        return ipStatusMapper.selectIpStatusList(ipStatus);
    }

    /**
     * 新增web服务运行ip
     * 
     * @param ipStatus web服务运行ip
     * @return 结果
     */
    @Override
    public int insertIpStatus(IpStatus ipStatus)
    {
        ipStatus.setCreateTime(DateUtils.getNowDate());
        return ipStatusMapper.insertIpStatus(ipStatus);
    }

    /**
     * 修改web服务运行ip
     * 
     * @param ipStatus web服务运行ip
     * @return 结果
     */
    @Override
    public int updateIpStatus(IpStatus ipStatus)
    {
        ipStatus.setUpdateTime(DateUtils.getNowDate());
        return ipStatusMapper.updateIpStatus(ipStatus);
    }

    /**
     * 删除web服务运行ip对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteIpStatusByIds(String ids)
    {
        return ipStatusMapper.deleteIpStatusByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除web服务运行ip信息
     * 
     * @param id web服务运行ipID
     * @return 结果
     */
    @Override
    public int deleteIpStatusById(Long id)
    {
        return ipStatusMapper.deleteIpStatusById(id);
    }
}
