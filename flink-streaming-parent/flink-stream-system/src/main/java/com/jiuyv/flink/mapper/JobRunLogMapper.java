package com.jiuyv.flink.mapper;

import java.util.List;
import com.jiuyv.flink.domain.JobRunLog;

/**
 * 运行任务日志Mapper接口
 * 
 * @author shu_k
 * @date 2021-04-01
 */
public interface JobRunLogMapper 
{
    /**
     * 查询运行任务日志
     * 
     * @param id 运行任务日志ID
     * @return 运行任务日志
     */
    public JobRunLog selectJobRunLogById(Long id);

    /**
     * 查询运行任务日志列表
     * 
     * @param jobRunLog 运行任务日志
     * @return 运行任务日志集合
     */
    public List<JobRunLog> selectJobRunLogList(JobRunLog jobRunLog);

    /**
     * 新增运行任务日志
     * 
     * @param jobRunLog 运行任务日志
     * @return 结果
     */
    public int insertJobRunLog(JobRunLog jobRunLog);

    /**
     * 修改运行任务日志
     * 
     * @param jobRunLog 运行任务日志
     * @return 结果
     */
    public int updateJobRunLog(JobRunLog jobRunLog);

    /**
     * 删除运行任务日志
     * 
     * @param id 运行任务日志ID
     * @return 结果
     */
    public int deleteJobRunLogById(Long id);

    /**
     * 批量删除运行任务日志
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteJobRunLogByIds(String[] ids);
}
