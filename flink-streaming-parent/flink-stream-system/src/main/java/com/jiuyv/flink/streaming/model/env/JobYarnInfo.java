package com.jiuyv.flink.streaming.model.env;


/**
 * @Description: Yarn模式
 * @author  shu_k
 * @date 2021年4月2日 下午3:12:29
 */
public class JobYarnInfo {

    private String id;

    private String status;
    
	public JobYarnInfo() {
		
	}

	public JobYarnInfo(String id, String status) {
		this.id = id;
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
