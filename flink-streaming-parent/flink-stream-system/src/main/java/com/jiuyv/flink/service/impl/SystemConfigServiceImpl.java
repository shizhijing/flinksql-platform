package com.jiuyv.flink.service.impl;

import java.util.List;
import com.jiuyv.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiuyv.flink.mapper.SystemConfigMapper;
import com.jiuyv.flink.domain.SystemConfig;
import com.jiuyv.flink.service.ISystemConfigService;
import com.jiuyv.flink.streaming.common.enums.SysConfigEnumType;
import com.jiuyv.flink.streaming.dto.SystemConfigDTO;
import com.jiuyv.flink.streaming.enums.DeployModeEnum;
import com.jiuyv.common.core.text.Convert;

/**
 * 系统配置Service业务层处理
 * 
 * @author shu_k
 * @date 2021-04-01
 */
@Service
public class SystemConfigServiceImpl implements ISystemConfigService 
{
    @Autowired
    private SystemConfigMapper systemConfigMapper;

    /**
     * 查询系统配置
     * 
     * @param id 系统配置ID
     * @return 系统配置
     */
    @Override
    public SystemConfig selectSystemConfigById(Long id)
    {
        return systemConfigMapper.selectSystemConfigById(id);
    }

    /**
     * 查询系统配置列表
     * 
     * @param systemConfig 系统配置
     * @return 系统配置
     */
    @Override
    public List<SystemConfig> selectSystemConfigList(SystemConfig systemConfig)
    {
        return systemConfigMapper.selectSystemConfigList(systemConfig);
    }

    /**
     * 新增系统配置
     * 
     * @param systemConfig 系统配置
     * @return 结果
     */
    @Override
    public int insertSystemConfig(SystemConfig systemConfig)
    {
        systemConfig.setCreateTime(DateUtils.getNowDate());
        return systemConfigMapper.insertSystemConfig(systemConfig);
    }

    /**
     * 修改系统配置
     * 
     * @param systemConfig 系统配置
     * @return 结果
     */
    @Override
    public int updateSystemConfig(SystemConfig systemConfig)
    {
        systemConfig.setUpdateTime(DateUtils.getNowDate());
        return systemConfigMapper.updateSystemConfig(systemConfig);
    }

    /**
     * 删除系统配置对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSystemConfigByIds(String ids)
    {
        return systemConfigMapper.deleteSystemConfigByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除系统配置信息
     * 
     * @param id 系统配置ID
     * @return 结果
     */
    @Override
    public int deleteSystemConfigById(Long id)
    {
        return systemConfigMapper.deleteSystemConfigById(id);
    }

	@Override
	public String getSystemConfigByKey(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getYarnRmHttpAddress() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getFlinkHttpAddress(DeployModeEnum deployModeEnum) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SystemConfigDTO> getSystemConfig(SysConfigEnumType sys) {
		// TODO Auto-generated method stub
		return null;
	}
}
