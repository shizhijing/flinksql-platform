package com.jiuyv.flink.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.jiuyv.common.annotation.Excel;
import com.jiuyv.common.core.domain.BaseEntity;

/**
 * 告警发送情况日志对象 flink_alart_log
 * 
 * @author shu_k
 * @date 2021-04-01
 */
public class AlartLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** job_config的id  如果0代表的是测试, */
    @Excel(name = "job_config的id  如果0代表的是测试,")
    private Long jobConfigId;

    /** job名 */
    @Excel(name = "job名")
    private String jobName;

    /** 消息内容 */
    @Excel(name = "消息内容")
    private String message;

    /** 1:钉钉 */
    @Excel(name = "1:钉钉")
    private Integer type;

    /** 1:成功 0:失败 */
    @Excel(name = "1:成功 0:失败")
    private Integer status;

    /** 失败原因 */
    @Excel(name = "失败原因")
    private String failLog;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer isDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setJobConfigId(Long jobConfigId) 
    {
        this.jobConfigId = jobConfigId;
    }

    public Long getJobConfigId() 
    {
        return jobConfigId;
    }
    public void setJobName(String jobName) 
    {
        this.jobName = jobName;
    }

    public String getJobName() 
    {
        return jobName;
    }
    public void setMessage(String message) 
    {
        this.message = message;
    }

    public String getMessage() 
    {
        return message;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setFailLog(String failLog) 
    {
        this.failLog = failLog;
    }

    public String getFailLog() 
    {
        return failLog;
    }
    public void setIsDeleted(Integer isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Integer getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("jobConfigId", getJobConfigId())
            .append("jobName", getJobName())
            .append("message", getMessage())
            .append("type", getType())
            .append("status", getStatus())
            .append("failLog", getFailLog())
            .append("isDeleted", getIsDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
