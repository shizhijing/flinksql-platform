package com.jiuyv.flink.mapper;

import java.util.List;
import com.jiuyv.flink.domain.IpStatus;

/**
 * web服务运行ipMapper接口
 * 
 * @author shu_k
 * @date 2021-04-01
 */
public interface IpStatusMapper 
{
    /**
     * 查询web服务运行ip
     * 
     * @param id web服务运行ipID
     * @return web服务运行ip
     */
    public IpStatus selectIpStatusById(Long id);

    /**
     * 查询web服务运行ip列表
     * 
     * @param ipStatus web服务运行ip
     * @return web服务运行ip集合
     */
    public List<IpStatus> selectIpStatusList(IpStatus ipStatus);

    /**
     * 新增web服务运行ip
     * 
     * @param ipStatus web服务运行ip
     * @return 结果
     */
    public int insertIpStatus(IpStatus ipStatus);

    /**
     * 修改web服务运行ip
     * 
     * @param ipStatus web服务运行ip
     * @return 结果
     */
    public int updateIpStatus(IpStatus ipStatus);

    /**
     * 删除web服务运行ip
     * 
     * @param id web服务运行ipID
     * @return 结果
     */
    public int deleteIpStatusById(Long id);

    /**
     * 批量删除web服务运行ip
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteIpStatusByIds(String[] ids);
}
