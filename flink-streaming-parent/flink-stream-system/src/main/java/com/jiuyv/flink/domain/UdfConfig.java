package com.jiuyv.flink.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.jiuyv.common.annotation.Excel;
import com.jiuyv.common.core.domain.BaseEntity;

/**
 * udf函数管理对象 flink_udf_config
 * 
 * @author shu_k
 * @date 2021-04-01
 */
public class UdfConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** udf函数名 */
    @Excel(name = "udf函数名")
    private String udfName;

    /** udf描述 */
    @Excel(name = "udf描述")
    private String udfDesc;

    /** 存储路径 */
    @Excel(name = "存储路径")
    private String udfPath;

    /** udf地址 */
    @Excel(name = "udf地址")
    private String udfUrl;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUdfName(String udfName) 
    {
        this.udfName = udfName;
    }

    public String getUdfName() 
    {
        return udfName;
    }
    public void setUdfDesc(String udfDesc) 
    {
        this.udfDesc = udfDesc;
    }

    public String getUdfDesc() 
    {
        return udfDesc;
    }
    public void setUdfPath(String udfPath) 
    {
        this.udfPath = udfPath;
    }

    public String getUdfPath() 
    {
        return udfPath;
    }
    public void setUdfUrl(String udfUrl) 
    {
        this.udfUrl = udfUrl;
    }

    public String getUdfUrl() 
    {
        return udfUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("udfName", getUdfName())
            .append("udfDesc", getUdfDesc())
            .append("udfPath", getUdfPath())
            .append("udfUrl", getUdfUrl())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
