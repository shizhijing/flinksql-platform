package com.jiuyv.flink.service.impl;

import java.util.Date;
import java.util.List;
import com.jiuyv.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiuyv.flink.mapper.SavepointBackupMapper;
import com.jiuyv.flink.domain.SavepointBackup;
import com.jiuyv.flink.service.ISavepointBackupService;
import com.jiuyv.common.core.text.Convert;

/**
 * savepoint备份地址Service业务层处理
 * 
 * @author shu_k
 * @date 2021-04-01
 */
@Service
public class SavepointBackupServiceImpl implements ISavepointBackupService 
{
    @Autowired
    private SavepointBackupMapper savepointBackupMapper;

    /**
     * 查询savepoint备份地址
     * 
     * @param id savepoint备份地址ID
     * @return savepoint备份地址
     */
    @Override
    public SavepointBackup selectSavepointBackupById(Long id)
    {
        return savepointBackupMapper.selectSavepointBackupById(id);
    }

    /**
     * 查询savepoint备份地址列表
     * 
     * @param savepointBackup savepoint备份地址
     * @return savepoint备份地址
     */
    @Override
    public List<SavepointBackup> selectSavepointBackupList(SavepointBackup savepointBackup)
    {
        return savepointBackupMapper.selectSavepointBackupList(savepointBackup);
    }

    /**
     * 新增savepoint备份地址
     * 
     * @param savepointBackup savepoint备份地址
     * @return 结果
     */
    @Override
    public int insertSavepointBackup(SavepointBackup savepointBackup)
    {
        savepointBackup.setCreateTime(DateUtils.getNowDate());
        return savepointBackupMapper.insertSavepointBackup(savepointBackup);
    }

    /**
     * 修改savepoint备份地址
     * 
     * @param savepointBackup savepoint备份地址
     * @return 结果
     */
    @Override
    public int updateSavepointBackup(SavepointBackup savepointBackup)
    {
        savepointBackup.setUpdateTime(DateUtils.getNowDate());
        return savepointBackupMapper.updateSavepointBackup(savepointBackup);
    }

    /**
     * 删除savepoint备份地址对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSavepointBackupByIds(String ids)
    {
        return savepointBackupMapper.deleteSavepointBackupByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除savepoint备份地址信息
     * 
     * @param id savepoint备份地址ID
     * @return 结果
     */
    @Override
    public int deleteSavepointBackupById(Long id)
    {
        return savepointBackupMapper.deleteSavepointBackupById(id);
    }

	@Override
	public String getSavepointPathById(Long id, Long savepointId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertSavepoint(Long id, String savepointPath, Date date) {
		// TODO Auto-generated method stub
		
	}
}
