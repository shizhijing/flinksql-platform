package com.jiuyv.system.mapper;

import java.util.List;
import com.jiuyv.system.domain.SysConfig;

/**
 * @Description: 参数配置 数据层
 * @author  shu_k
 * @date 2021年3月30日 下午1:38:55
 */
public interface SysConfigMapper {
	/**
	 * 查询参数配置信息
	 * 
	 * @param config 参数配置信息
	 * @return 参数配置信息
	 */
	public SysConfig selectConfig(SysConfig config);

	/**
	 * 查询参数配置列表
	 * 
	 * @param config 参数配置信息
	 * @return 参数配置集合
	 */
	public List<SysConfig> selectConfigList(SysConfig config);

	/**
	 * 根据键名查询参数配置信息
	 * 
	 * @param configKey 参数键名
	 * @return 参数配置信息
	 */
	public SysConfig checkConfigKeyUnique(String configKey);

	/**
	 * 新增参数配置
	 * 
	 * @param config 参数配置信息
	 * @return 结果
	 */
	public int insertConfig(SysConfig config);

	/**
	 * 修改参数配置
	 * 
	 * @param config 参数配置信息
	 * @return 结果
	 */
	public int updateConfig(SysConfig config);

	/**
	 * 批量删除参数配置
	 * 
	 * @param configIds 需要删除的数据ID
	 * @return 结果
	 */
	public int deleteConfigByIds(String[] configIds);
}