package com.jiuyv.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @Description: 角色和部门关联 sys_role_dept
 * @author  shu_k
 * @date 2021年3月30日 下午1:36:54
 */
public class SysRoleDept {
	/** 角色ID */
	private Long roleId;

	/** 部门ID */
	private Long deptId;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getDeptId() {
		return deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("roleId", getRoleId())
				.append("deptId", getDeptId()).toString();
	}
}
