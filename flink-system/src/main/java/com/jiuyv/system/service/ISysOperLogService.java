package com.jiuyv.system.service;

import java.util.List;
import com.jiuyv.system.domain.SysOperLog;

/**
 * @Description: 操作日志 服务层
 * @author  shu_k
 * @date 2021年3月30日 下午1:51:23
 */
public interface ISysOperLogService {
	/**
	 * 新增操作日志
	 * 
	 * @param operLog 操作日志对象
	 */
	public void insertOperlog(SysOperLog operLog);

	/**
	 * 查询系统操作日志集合
	 * 
	 * @param operLog 操作日志对象
	 * @return 操作日志集合
	 */
	public List<SysOperLog> selectOperLogList(SysOperLog operLog);

	/**
	 * 批量删除系统操作日志
	 * 
	 * @param ids 需要删除的数据
	 * @return 结果
	 */
	public int deleteOperLogByIds(String ids);

	/**
	 * 查询操作日志详细
	 * 
	 * @param operId 操作ID
	 * @return 操作日志对象
	 */
	public SysOperLog selectOperLogById(Long operId);

	/**
	 * 清空操作日志
	 */
	public void cleanOperLog();
}
