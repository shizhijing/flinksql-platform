package com.jiuyv.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiuyv.common.core.text.Convert;
import com.jiuyv.system.domain.SysLogininfor;
import com.jiuyv.system.mapper.SysLogininforMapper;
import com.jiuyv.system.service.ISysLogininforService;

/**
 * @Description: 系统访问日志情况信息 服务层处理
 * @author  shu_k
 * @date 2021年3月30日 下午1:57:21
 */
@Service
public class SysLogininforServiceImpl implements ISysLogininforService {

	@Autowired
	private SysLogininforMapper logininforMapper;

	/**
	 * 新增系统登录日志
	 * 
	 * @param logininfor 访问日志对象
	 */
	@Override
	public void insertLogininfor(SysLogininfor logininfor) {
		logininforMapper.insertLogininfor(logininfor);
	}

	/**
	 * 查询系统登录日志集合
	 * 
	 * @param logininfor 访问日志对象
	 * @return 登录记录集合
	 */
	@Override
	public List<SysLogininfor> selectLogininforList(SysLogininfor logininfor) {
		return logininforMapper.selectLogininforList(logininfor);
	}

	/**
	 * 批量删除系统登录日志
	 * 
	 * @param ids 需要删除的数据
	 * @return
	 */
	@Override
	public int deleteLogininforByIds(String ids) {
		return logininforMapper.deleteLogininforByIds(Convert.toStrArray(ids));
	}

	/**
	 * 清空系统登录日志
	 */
	@Override
	public void cleanLogininfor() {
		logininforMapper.cleanLogininfor();
	}
}
