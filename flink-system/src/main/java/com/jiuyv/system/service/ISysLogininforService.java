package com.jiuyv.system.service;

import java.util.List;
import com.jiuyv.system.domain.SysLogininfor;

/**
 * @Description: 系统访问日志情况信息 服务层
 * @author  shu_k
 * @date 2021年3月30日 下午1:50:37
 */
public interface ISysLogininforService {
	/**
	 * 新增系统登录日志
	 * 
	 * @param logininfor 访问日志对象
	 */
	public void insertLogininfor(SysLogininfor logininfor);

	/**
	 * 查询系统登录日志集合
	 * 
	 * @param logininfor 访问日志对象
	 * @return 登录记录集合
	 */
	public List<SysLogininfor> selectLogininforList(SysLogininfor logininfor);

	/**
	 * 批量删除系统登录日志
	 * 
	 * @param ids 需要删除的数据
	 * @return
	 */
	public int deleteLogininforByIds(String ids);

	/**
	 * 清空系统登录日志
	 */
	public void cleanLogininfor();
}
