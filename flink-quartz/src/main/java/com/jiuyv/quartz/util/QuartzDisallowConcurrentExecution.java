package com.jiuyv.quartz.util;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import com.jiuyv.quartz.domain.SysJob;

/**
 * @Description: 定时任务处理（禁止并发执行）
 * @author  shu_k
 * @date 2021年3月30日 下午3:08:57
 */
@DisallowConcurrentExecution
public class QuartzDisallowConcurrentExecution extends AbstractQuartzJob {
	@Override
	protected void doExecute(JobExecutionContext context, SysJob sysJob) throws Exception {
		JobInvokeUtil.invokeMethod(sysJob);
	}
}
