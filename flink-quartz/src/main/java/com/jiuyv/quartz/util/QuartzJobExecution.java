package com.jiuyv.quartz.util;

import org.quartz.JobExecutionContext;
import com.jiuyv.quartz.domain.SysJob;

/**
 * @Description: 定时任务处理（允许并发执行）
 * @author  shu_k
 * @date 2021年3月30日 下午3:09:18
 */
public class QuartzJobExecution extends AbstractQuartzJob {
	@Override
	protected void doExecute(JobExecutionContext context, SysJob sysJob) throws Exception {
		JobInvokeUtil.invokeMethod(sysJob);
	}
}
